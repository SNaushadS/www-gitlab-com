---
layout: markdown_page
title: "HashiCorp Terraform"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Positioning
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Terraform by HashiCorp,is a tool for building, changing, and versioning infrastructure. It allows users to define a datacenter infrastructure in a high-level configuration language, from which it can create an execution plan to build the infrastructure such as OpenStack or in a service provider.  Written in Go, it has a simple syntax that allows modularity and works against multi-cloud and allows for the automation of infrastructure.

GitLab is a single appliation for the whole DevOps lifecylce that includes not only configuration management, but also capabilities for proejct management, source code management, CI/CD, and monitoring. GitLab is designed for Kubernetes and cloud native applications.

## Resources
* [HashiCorp Terraform](https://www.terraform.io/)
* [HashiCorp Terraform Wikipedia](https://en.wikipedia.org/wiki/Terraform_(software)) 