---
layout: markdown_page
title: "Azure DevOps Overview"
---

## Azure DevOps Consists of
(***bold italics text*** = GitLab currently falls short in this area)

### Pipelines (previously in VSTS, TFS)
* Includes previous Release Manager (release pipelines)
* Native container support
* Save to any container registry
* ***Linux, MacOS, Windows cloud hosted agents***
* ***Deployment stages, release gates, and approvals***
* GitHub Marketplace integration - makes it easy to setup and run CI from GitHub.
* ***100's of 3rd party integrations***
* ***Design pipelines in yml or UI***

#### Costs
* public projects get 10 free parallel jobs, unlimited time
* private projects - MS hosted - Free 1 parallel job, 1800 mins/mnth - $40/parallel addition, unlimited time
* private projects - self-hosted - Free parallel job, unlimited, +$15 each additional

#### Screenshots

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-pipeline-1.png" target="_blank">
    <img src="./images/ss-pipeline-1.png" alt="Pipeline 1" style="width:85%;">
  </a>
</center></body>

<br>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-pipeline-2.png" target="_blank">
    <img src="./images/ss-pipeline-2.png" alt="Pipeline 2" style="width:85%;">
  </a>
</center></body>

<br>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-pipeline-3.png" target="_blank">
    <img src="./images/ss-pipeline-3.png" alt="Pipeline 3" style="width:85%;">
  </a>
</center></body>

<br>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-pipeline-release-4.png" target="_blank">
    <img src="./images/ss-pipeline-release-4.png" alt="Release Pipeline 4" style="width:85%;">
  </a>
</center></body>

### Boards (previously in VSTS, TFS)
* Work/issue tracking
* Backlogs
* ***Team dashboards***
* ***Custom reporting***
* Kanban boards
* Scrum boards and sprint planning
* Customizable work item workflows

#### Costs
* Free <=5 users
* Included in $30/mnth per 10 users

#### Screenshots

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-boards-1.png" target="_blank">
    <img src="./images/ss-boards-1.png" alt="Boards 1" style="width:85%;">
  </a>
</center></body>

<br>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-boards-2.png" target="_blank">
    <img src="./images/ss-boards-2.png" alt="Boards 2" style="width:85%;">
  </a>
</center></body>

### Artifacts (previously in VSTS, TFS)
* ***Maven, npm, and NuGet package feeds from public and private sources***
* ***Caching proxy of external repos/feeds***
* Artifacts integrate natively with pipelines

#### Costs
* Free <=5 users
* additional $4/user above 5

#### Screenshots

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-artifacts-1.png" target="_blank">
    <img src="./images/ss-artifacts-1.png" alt="Artifacts 1" style="width:85%;">
  </a>
</center></body>

### Repos (previously in VSTS, TFS)
* Unlimited private Git repo hosting
* Diff in-line threaded code reviews
* Branch policies defining merge
* Pull requests
* Semantic code search
* Webhooks and REST APIs
* ***Support for TFVC*** (and nobody cares)

#### Costs
* Free <=5 users
* Included in $30/mnth per 10 users

#### Screenshots

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-repos-1.png" target="_blank">
    <img src="./images/ss-repos-1.png" alt="Repos 1" style="width:85%;">
  </a>
</center></body>

<br>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-repos-2.png" target="_blank">
    <img src="./images/ss-repos-2.png" alt="Repos 2" style="width:85%;">
  </a>
</center></body>

### Test Plans (previously in VSTS, TFS)
* ***Test & Feedback (exploratory/manual testing) - capture & record issues***
* ***Test planning, tracking & execution***
* ***Load testing (Azure DevOps and VSTS only)***
* User acceptance testing
* Centralized reporting

#### Costs
* additional $52/mnth per user

#### Screenshots

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-test-1.png" target="_blank">
    <img src="./images/ss-test-1.png" alt="Test 1" style="width:85%;">
  </a>
</center></body>

<br>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body><center>
  <a href="./images/ss-test-2.png" target="_blank">
    <img src="./images/ss-test-2.png" alt="Test 2" style="width:85%;">
  </a>
</center></body>

### Separate from Azure DevOps but available for extra cost
* Azure Monitor - APM, infra, data, services **(GitLab has)**
    * App centric
    * but separate from Azure DevOps.
    * [https://docs.microsoft.com/en-us/azure/azure-monitor/overview](https://docs.microsoft.com/en-us/azure/azure-monitor/overview)
    * pay by use
* Visual Studio - Full blown IDE - Free with sub
* Visual Studio Code - IDE Lite - Free  **(GitLab has)**
* Container Registry (MS has Azure Container Registry) **(GitLab has)**

### Unavailable as part of Microsoft offering
(but which ***GitLab has***)
* Security scanning built-in
* Review Apps
* Deployment Scenarios (canary, incremental, etc)
* Feature Flags
* ChatOps

## General Notes
* All paid plans include unlimited stakeholder users who can view and contribute to work items and boards, and view dashboards, charts, and pipelines
* Release details and [roadmap](https://docs.microsoft.com/en-us/azure/devops/release-notes/)
* All current VSTS subscribers will be moved automatically to Azure DevOps.
* TFS (on prem) pricing implies a SaaS first mentality and customer push
    * Buy at least one Visual Studio license + Azure DevOps users @ $6/mnth
        * Visual Studio Professional ($45/mnth) - no Test Manager, Artifacts, Pipelines (unless OSS)
        * Visual Studio Enterprise ($250/mnth) - include Test Manager and Artifacts
    * [https://visualstudio.microsoft.com/team-services/tfs-pricing/](https://visualstudio.microsoft.com/team-services/tfs-pricing/)
