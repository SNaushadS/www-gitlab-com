---
layout: job_family_page
title: "Solutions Architect"
---

Solution Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements. Solution Architects are responsible for actively driving and managing the technology evaluation and validation stages of the sales process. Solution Architects are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the the technical solution while also understanding the business challenges the customer is trying to overcome.

The Solution Architect for Customer Success Initiatives provides the opportunity to help drive value and change in the world of software development for one of the fastest growing platforms. By applying your solution selling and architecture experience from planning to monitoring, you will support and enable successful adoption of the GitLab platform. You will be working directly with our top enterprise customers. You will be working collaboratively with our sales, engineering, product management and marketing organizations.

This role provides technical guidance and support through the entire sales cycle. You will have the opportunity to help shape and execute a strategy to build mindshare and broad use of the GitLab platform within enterprise customers becoming the trusted advisor. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting. The ability to connect technology with measurable business value is critical to a solutions architect. You should also have a demonstrated ability to think strategically about business, products, and technical challenges.

To learn more, see the [Solutions Architect handbook](/handbook/customer-success/solutions-architects)

### Responsibilities

* Primarily engaged in a technical consultancy role, providing technical assistance and guidance specific to enterprise level implementations, during the pre-sales process by identifying customers technical and business requirements in order to design a custom GitLab solution
* In partnership with the sales team, formulate and execute a sales strategy to exceed revenue objectives through the adoption of GitLab
* Educate customers of all sizes on the value proposition of GitLab, and participate in all levels of discussions throughout the organization to ensure our solution is set up for successful deployment
* Work on­site with strategic, enterprise­ class customers, delivering solutions architecture consulting, technical guidance, knowledge transfer and establish “trusted advisor status”
* Capture and share best-practice knowledge amongst the GitLab community
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, diagrams or the GitLab Handbook
* Build deep relationships with senior technical individuals within customers to enable them to be GitLab advocates
* Serve as the customer advocate to other GitLab teams including Product Development, Sales and Marketing
* Present GitLab platform strategy, concepts and roadmap to technical leaders within customer organizations

### Requirements

* Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers
* Strong written communication skills
* High level of comfort communicating effectively across internal and external organizations
* Ideal candidates will preferably have 7 plus years IT industry or IT technical sales related to experience with a proven track record of solution sales expertise
* Significant experience with executive presence and a proven ability to lead and facilitate executive meetings and workshops
* Deep knowledge of software development lifecycle and development pipeline (planning to monitoring)
* Understanding of continuous integration, continuous deployment, chatOps and cloud native
* Experience with waterfall, Agile (SCRUM, Kanban, etc) and able to discuss workflows for different software development processes
* Experience with modern development practices preferred:
  * Kubernetes
  * Docker
  * Linux
  * Package Management
  * DevOps Pipelines (CI/CD)
  * Application Security (SAST, DAST)
  * Cloud (AWS, GCP, Azure)
  * Application Performance Monitoring
  * System Logging
* Experience with several of the following tools preferred:
  * Ruby on Rails, Java, PHP, Python
  * Git, BitBucket, GitHub, SVN
  * Jira, VersionOne, TFS
  * Jenkins, Travis, CircleCI
  * Veracode, Fortify
  * Artifactory, Nexus
  * New Relic, Nagios
* Understand mono-repo and distributed-repo approaches
* Understand BASH / Shell scripting
* Ability to travel up to 35%
* B.Sc. in Computer Science or equivalent experience
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab.

### Federal

#### Additional Responsibilities

* Participating in conferences and trade shows and interacting with government customers in attendance
* Contribute to the creation of case studies, white papers, and media articles for government customers and/or partner
* Consistently provide world-class customer service during pre-sales, implementation, and post-sales activities
* Manage all technical aspects of the sales cycle: creating high quality professional presentations, custom demos, proof of concepts, deliver technical deep dive sessions & workshops, differentiate GitLab from competition, answering RFI, RFPs, etc.
* TS/SCI Security Clearance

#### Additional Requirements

* Washington DC area preferred but not required
* Knowledge and at least 4 years of experience with Federal customers
* Ability to travel up to 50%
* Understand mono-repo and distributed-repo approaches

## Manager, Solutions Architects

GitLab is a hyper growth company searching for people who are intelligent, aggressive and agile with strong skills in technology, sales, business, communication and leadership. Desire to lead through change is a must.

The Manager, Solutions Architects role is a management position on the front lines. This is a player/coach role where the individual is expected to be experienced in and have advanced insight to the GitLab platform. The individual will contribute to territory and account strategy as well as driving the execution directly and indirectly. The individual will need to be very comfortable giving and receiving positive and constructive feedback, as well as adapting to environmental change and retrospecting on successes and failures. The Manager, Solutions Architects will work together with the other managers within the customer success organization to help execute on strategies and vision with the Director.

### Responsibilities

- Oversee the day-to-day operations of your team of Solutions Architects
- Work as an individual contributor for key accounts as needed or requested
- Participate in technical sales calls, and contribute to creating sales and evaluation strategy at the account level
- Provide technical leadership and platform strategy to the team as well as the larger sales organization
- All aspects of people management, including hiring, development, training, regular 1:1s and annual reviews
- Hire, train and retain top talent
- Provide feedback and direction, both positive and constructive, to personnel in a timely manner
- Build a strong team and provide satisfaction among your team and customers
- Influence both internal and external C-level executives through presentations and whiteboard sessions
- Work collaboratively with other groups, including Sales, Professional Services, Support, Engineering, Technical Account - Managers and Product Management, to ensure effective operation of your team, achieve the technical win, and ensure ongoing customer satisfaction
- Formulate best practices for presentations, demos, and POCs as well as overall sales strategy
- Act as a trusted advisor to higher level management on strategic opportunity reviews, emerging competitive threats, product direction, and establishing sales objectives and strategies
- Work with the Customer Success Director to help establish and manage Solutions Architect goals and responsibilities
- Assist in development of thought leadership, event-specific and customer-facing presentations
- Share hands-on technical preparation and presentation work for key accounts
- Take the lead in hiring and onboarding new SA’s
- Ensure the SA team exceeds corporate expectations in core knowledge, communication and execution
- Define and maintain a high bar for team member expectations and enable the team to achieve it
- Challenge the team and yourself to continually learn and grow as trusted advisors to clients
- Evangelize new product features & provide customer feedback to GitLab product management and engineering groups
- Ensuring the Solutions Architects engage the Technical Account Manager during the pre-sales so that  the post-sales technical process handoff for seamless Drive customer adoption and success by providing oversight, adoption - recommendations, opportunities for greater service
- Ensure timely resolution of POC issues by coordinating support responses
- Maintain a deep understanding of each customer’s business as well as their technical environments -- become a trusted advisor to our customers
- Support the Sales team with opportunity qualification, demonstrations, Proof of Concept presentations (POC), RFP responses, and business justification in a pre-sales capacity
- Mentor and develop the existing Solutions Architect team
- Be able to present complex technical solutions to clients
- Present credibly to engineering decision makers and technology executives
- Develop and execute solution selling strategies and identify opportunities for process improvements
- Remain knowledgeable and up-to-date on GitLab releases

### Requirements

All requirements for the Solutions Architect role apply to the Manager, Solutions Architects position, as the management position will need to understand and participate with the day-to-day aspects of the Solutions Architect role in addition to managing the team. Additional requirements include:

- Experienced in and with advanced insight to the GitLab platform
- Experienced in giving and received positive and constructive feedback
- Able to adapt to environmental change and restrospecting on success and failures
- Previous leadership experience is a plus
- Experienced in collaborating with other managers and executing strategies

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with Director of Customer Success
- For an individual contributor role, candidates will be invited to schedule an interview with a Customer Success peer and may be asked to participate in a demo of a live install of GitLab
- For a manager role, candidates will be invited to schedule interviews with our Regional Sales Directors for North America
- Candidates will be invited to schedule a third interview with our CRO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer directly from Director of Customer Success

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).

## Compensation

You will typically get 80% as base, and 20% based on meeting the global sales goal of incremental ACV. Also see the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/).
