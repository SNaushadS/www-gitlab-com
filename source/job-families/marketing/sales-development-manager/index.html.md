---
layout: job_family_page
title: "Sales Development Manager"
---

As a Sales Development Manager, you are a player-coach. As a Manager your job is threefold: (1) Lead from the front and  generate qualified opportunities for the sales team, (2) Train other members of the sales development team, and (3) take on operational and administrative tasks to help the sales development team perform and exceed expectations. You will be a source of knowledge and best practices amongst the outbound SDRs, and will help to train, onboard, and mentor new SDRs.

## Responsibilities

* Train other members of the Sales Development Team to identify, contact, and create qualified opportunities.
* Ensure Sales Development Team members improve performance and abilities over time by providing coaching and feedback in recurring 1:1s
* Assist with recruiting, hiring, and onboarding new Sales Development Representatives.
* Work closely with the Online Marketing Manager on targeted ad campaigns for the team’s account list
* Work closely with the Sales and Business Development Manager to improve opportunity management and qualification processes
* Work closely with the Sales and Business Development Manager as well as the Regional Sales Directors (RDs) to identify key company accounts to develop.
* Work in collaboration with Content and Product Marketing to develop effective messaging for outbound communications to your assigned accounts.

## Requirements

* Excellent spoken and written English
* Experience in sales, marketing, or customer service for a technical product - leadership experience is highly preferred.
* Experience with CRM software (Salesforce preferred)
* Experience in sales operations and/or marketing automation software preferred
* Understanding of B2B software, Open Source software, and the developer product space is preferred
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Passionate about technology and learning more about GitLab
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values), and work in accordance with those values.

**Note on Compensation**

See the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/) to understand OTE salary.
