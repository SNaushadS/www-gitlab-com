function setupContributeCountdown() {
  var nextSummitDate = new Date('May 8, 2019 08:00:00').getTime();

  var x = setInterval(function() {
    var now = new Date().getTime();

    var distance = nextSummitDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById('nextSummitCountdown').innerHTML = days + ' days ' + hours + ' hours '
    + minutes + ' minutes ' + seconds + ' seconds ';

    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById('nextSummitCountdown').innerHTML = 'Already happened!';
    }
  }, 1000);
}

function setupVideo() {
  var videoButton = document.getElementById('video-button');
  var iframeContainer = document.getElementById('iframe-container');
  var iframeVideo = document.getElementById('iframe-video');
  var videoSource = 'https://www.youtube.com/embed/TExMuUjDg6I?autoplay=1';

  function showVideo() {
    iframeContainer.style.display = 'flex';
    iframeVideo.src = videoSource;
  }

  function hideVideo() {
    iframeContainer.style.display = 'none';
    iframeVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  videoButton.addEventListener('click', showVideo);
  iframeContainer.addEventListener('click', hideVideo);
  iframeVideo.addEventListener('click', stopProp);
}


(function() {
  setupVideo();
  setupContributeCountdown();
})();

