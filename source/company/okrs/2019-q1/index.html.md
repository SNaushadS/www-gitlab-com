---
layout: markdown_page
title: "2019 Q1 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. More leads. Successful customers (standard implementation path, customer success based on their goals, DevOps maturity), Scalable marketing (Pipe-to-spend for all, double down on what works, more experiments with things like demo's, call us, and ebooks)

* Alliances: grow leads, customers, and scalable marketing
    * 3 public references for each major public/private cloud
    * Increased customer purchase and growth through marketplaces
    * Joint webinars with primary partners - 200+ leads per webinar
* CMO: Recharge Inbound Marketing Engine. Top 10 Gitlab.com Pages re-architected for inbound, Inbound trial and contact-us strategy documented and baselined, Pages optimized for top 20 non-branded SEO target terms, Inbound strategy for Dev, Sec & Ops personas launched
* CMO: Achieve marketing process and visibility excellence. 100% visibility into any and all leads at any time, Full-funnel marketing dashboard published in Looker, Unified marketing calendar published.
* CMO: Achieve $18m in IACV pipeline (3x target) at beginning of Q2 FY19. 800 SAOs delivered to sales (SAOs as currently defined), X# of contact sales MQLS, Y# of EE trials.

### CEO: Popular next generation product. Triple secure value (3 stages, multiple teams). Grown use of stages (SMAU). iPhone app on iPad.

* Product:
    * Increase product breadth. 25% (6 of 22) "new in 2019" categories at `minimal` maturity, at least one MVC for one new role (e.g. Designers), an impactful demo of developing an iPhone app on GitLab.com.
    * Increase product depth for existing categories. 50% (11 of 22) "new in 2018" categories at `complete` maturity. 
    * Grow use of GitLab for all stages of the DevOps lifecycle. Increase Stage Monthly Active Users ([SMAU](/handbook/product/growth/#smau)) 10% m/m for each stage, 6 reference customers using all stages concurrently.
* Alliances: write whitepaper and evaluate qualifying for security competancy with major clouds 
* Engineering: GitLab.com ready for mission critical custom workloads: 99.95% Availability
  * Development: 
  * Infrastructure: 
  * Quality: 
  * Security: 
  * Support: 
  * UX: 

### CEO: Great team. Employed brand (known for all remote, great communication of total compensation, 10 videos per manager and up), Effective hiring (Faster apply to hire), ELO score per interviewer), Decision making effectiveness (kpis from original source and red/green, training for director group)
* CFO: Improve financial reporting and accounting processes to support growth and increased transparency.
    * Manager of Data Team: All executive dashboards completed with goals and definitions 
    * Manager of Data Team: Public release of finance metric(s).  Data Integrity Process (DIP) completed for ARR, Net and Gross Retention and Customer counts.
    * FinOps Lead: Integrated financial model that covers 100% of expense categories driven by IACV (as first iteration) and marketing funnel (as second iteration).
    * FinOps Lead: Release and change control process for financial reporting.
    * Sr Dir. of Legal: Detailed SoX compliance plan published internally and reviewed by Board. 
    * Sr Acctg Manager:  Key internal controls documented in handbook and functioning effectively
* CFO: Create scalable infrastructure for achieving headcount growth
    * Sr Dir. of Legal: 90% of team members covered by scalable employment solution
    * Payroll and Payments Lead: Automate contractor payroll
* CFO: Improve company wide operational processes
    * Director of Bus Ops: Roadmaps for all staffed business operations functions are shipped and Q2 iteration by EOQ
    * Controller: Zuora upgrade to orders which will allow for ramped deals, multiple amendments on single quote and MRR by subscription reporting.

* Engineering: Customer focus: Get every engineer in at least one customer meeting or support case
* Engineering: Unify vacancy descriptions and experience factor content for all engineering roles
  * Development: 
  * Infrastructure: 
  * Quality: 
  * Security: 
  * Support: 
  * UX: 


