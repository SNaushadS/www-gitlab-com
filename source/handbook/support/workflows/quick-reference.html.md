# Purpose

We know finding information for issues can be tough as a new support team member given the amount of information available. For this reason, we've created a list of common support requests as a navigation guide. This reference includes links to existing reference material, "live" ticket examples and special handling notes. 

Below these references, you'll find detailed workflows for specific scenarios you might encounter. If there's ever a conflict between what is documented in the quick reference or what was done in a historical ticket and a workflow, the workflow is authoritative (and you should consider submitting an MR to bring things into line!)

## General Finance Requests
*  **Example Tickets**
    *  [Migration from Self-Managed to GitLab.com + Invoice Request](https://gitlab.zendesk.com/agent/tickets/106682)
    *  [Needs an invoice & wants to change credit card on file](https://gitlab.zendesk.com/agent/tickets/107717)
    * [Wants monthly billing](https://gitlab.zendesk.com/agent/tickets/104252) 
    * [Purchased (& was charged) for a paid plan but not applied to account](https://gitlab.zendesk.com/agent/tickets/107783)
*  **Special Handling Notes:**
*  **Reference Material**

## Account Access - GitLab.com
*  **Example Tickets**
    *  [User is setting up new machine and can't find 2FA keys](https://gitlab.zendesk.com/agent/tickets/108183)
    *  [User is connecting to GitLab via GitHub & has lost OTP codes](https://gitlab.zendesk.com/agent/tickets/108139)
    *  [User cannot be found in GitLab.com admin](https://gitlab.zendesk.com/agent/tickets/107216)
    *  [Password Reset](https://gitlab.zendesk.com/agent/tickets/109121)
*  **Special Handling Notes:**
*  **Reference Material**
    * [Account Verification](services/gitlab_com/account_verification.html)

## Change Plan & General Account Clarification - GitLab.com
*  **Example Tickets**
    *  Purchase while on trial 
        * [Example 1](https://gitlab.zendesk.com/agent/tickets/106633)
        * [Example 2](https://gitlab.zendesk.com/agent/tickets/109097)
    *  [Wants a quote for purchase ](https://gitlab.zendesk.com/agent/tickets/109025)
    *  Clarification between personal subscription and group subscription
        * [Example 1](https://gitlab.zendesk.com/agent/tickets/107993)
        * [Example 2](https://gitlab.zendesk.com/agent/tickets/106799)
    *  [Upgrade from one paid plan to another paid plan](https://gitlab.zendesk.com/agent/tickets/109015)
    *  [User accidentally upgraded plan for personal account instead of group ](https://gitlab.zendesk.com/agent/tickets/107896)
    *  [Wants to cancel/refund, IS within 45 days of purchase ](https://gitlab.zendesk.com/agent/tickets/108210)
    *  Wants to cancel/refund, is NOT within 45 days of purchase 
        * [Example 1](https://gitlab.zendesk.com/agent/tickets/108350)
        * [Example 2](https://gitlab.zendesk.com/agent/tickets/108372)
*  **Special Handling Notes:** 
    * When sending a request to the Accounts Teams, use the ZD Macro `General::Accounts Receivable` to move this into their queue.
    * When a customer already subscribes to a paid plan and they want to upgrade to another paid plan, request must be sent to Upgrades and Renewals. In this case, use the ZD Macro `General::Upgrades and Renewals`.
*  **Reference Material**
    * [Handling Refund Requests](shared/support_workflows/handling_refund_requests.html)

## Dormant Username - GitLab.com
*  **Example Tickets**
    *  [Requested username is deemed Active, contacted owner successfully](https://gitlab.zendesk.com/agent/tickets/107087)
    *  [Requested username is deemed Active, request denied](https://gitlab.zendesk.com/agent/tickets/103076)
    *  Requested username is deemed Inactive, request approved
        * [Example 1](https://gitlab.zendesk.com/agent/tickets/104815)
        * [Example 2](https://gitlab.zendesk.com/agent/tickets/102165)
        * [Example 3 - unconfirmed account created >90 days](https://gitlab.zendesk.com/agent/tickets/106568)
*  **Special Handling Notes:**
    * [Ticket status clarification](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1369#note_113800514), when sending a request to the username owner, set original ZD ticket status to `On-Hold` and new ticket (to usename owner) to `Pending`
*  **Reference Material**
    * [Dormant Username Policy](services/gitlab_com/dormant_username_policy.html)

## Security, DMCA & Spam - GitLab.com
*  **Example Tickets**
    *  [Blocked for hosting content that was flagged as malicious by Google Webmaster.](https://gitlab.zendesk.com/agent/tickets/105774)
    *  [Questions about SOC and ISO Compliance ](https://gitlab.zendesk.com/agent/tickets/108708)
    * [DMCA Complaint](https://gitlab.zendesk.com/agent/tickets/108878)
    * [Determining if Spam](https://gitlab.zendesk.com/agent/tickets/109094) [Slack Convo](https://gitlab.slack.com/archives/C4XFU81LG/p1543328690525500)
*  **Special Handling Notes:** 
    * For blocked users, contact Security on Slack ([example](https://gitlab.slack.com/messages/C248YCNCW/convo/C248YCNCW-1539730820.000100/?)) to find out _why_ user was blocked and if the block can be removed.
*  **Reference Material**
    * [Handling Misdirects from Security](services/support_workflows/handling_security_misdirects.html)
    * [Reinstating blocked accounts](services/gitlab_com/reinstating-blocked-accounts.html)
    * [Managing Spam](services/gitlab_com/managing_spam.html)
    * [Working with Security](services/support_workflows/working_with_security.html)


## Tools Used by Request Type

| Request               | [Risk Factor Worksheet](https://docs.google.com/spreadsheets/d/1NBH1xaZQSwdQdJSbqvwm1DInHeVD8_b2L08-V1QG1Qk/edit?usp=sharing) | [API Username Lookup](https://gitlab.com/api/v4/users?search=emal@email.com) | [GitLab.com Admin](https://gitlab.com/admin/users?utf8=%E2%9C%93&search_query=marcus.atkins%40ezyvet.com) | [Customers Portal - Admin](https://customers.gitlab.com/admin/) | [Mailgun](https://app.mailgun.com/app/logs/mg.gitlab.com?date_from=2018-11-08T00%3A00%3A00.000Z&date_to=2018-11-14T23%3A59%3A59.999Z&sort=datetime%3Adesc) | 
| ------- |:------:|:------:|:------:|:------:|:------:| 
| 2FA | X | X | X |   |   |  
| Password Reset |   | X | X |   | X | 
| Invoice Request |   |   |   | X |   |   
| Change Credit Card |   |   |   | X |   | 
| Cancel Trial |   |    |   | X |   |   
| Cancel + Refund |   |    |   | X |   |   
| Dormant Username |   | X | X |   |   | 
| Blocked by Security |   |   |   |   |   | 
|                     |   |   |   |   |   |


**TOOLS TIPS**
* [API Username Lookup](https://gitlab.com/api/v4/users?search=emal@email.com): Use in Firefox for an easier to read, parsed output 
* [Customers Portal - Admin](https://customers.gitlab.com/admin/): Shared login, password in vault. 
* [Mailgun](https://app.mailgun.com/app/logs/mg.gitlab.com): Shared login, password in vault. Make sure you switch the log view to `mg.gitlab.com`



---
