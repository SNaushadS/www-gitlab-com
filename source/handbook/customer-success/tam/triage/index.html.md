---
layout: markdown_page
title: "Account Triage"
---
## Technical Account Manager Handbook Pages
{:.no_toc}

- Account Triage *(Current)*
- [Technical Account Manager Summary](https://about.gitlab.com/handbook/customer-success/tam/)
- [Account Engagement](https://about.gitlab.com/handbook/customer-success/tam/engagement)
- [Account Onboarding](https://about.gitlab.com/handbook/customer-success/tam/onboarding)

---

# Account Triaging

In order to prevent customer churn and improve retention of our customers, we have an [Account Triage](https://gitlab.com/gitlab-com/customer-success/account-triage) project in GitLab. 

## Account Triage Project

If customer at risk of non-renewal, i.e. with a health score of Amber or Red in SFDC, create an issue in the [Account Triage](https://gitlab.com/gitlab-com/customer-success/account-triage) project.

All TAM's meet weekly to review the issue board for current "at risk" account activity. The meeting is a peer review of new at risk accounts to make sure activities are followed up on and owned across the business and is also a chance to discuss tactics/strategies on those accounts in case anything different should be done. 

The triage team also needs to check the health score report to ensure that all Amber and Red accounts have indeed been added to the board, and to make sure that all issues in the project have the correct health score according to SFDC. If the scores do not match, reach out to the TAM to find out why and rectify it.

Generally, this account is for Amber and Red accounts. If a TAM has an account that is Yellow, and feels they need help and support on that account from their team / the triage team described below, then they may add an issue on the triage board for that account.

When an account is added to the issue board, assign it with a 'triage team', which should include: 
* Account Owner
* Technical Account Manager
* Manager of Customer Experience and/or Director of Customer Success
* Regional Sales Manager for Account
* Solutions Architect where appropriate
* Implementation Engineer where appropriate
* Executive Sponsor (probably the account owner's RD, but could be VP of Engineering for example, depending on the problems the customer is having)
* Product Manager(s) where appropriate

This team will have responsibility for managing the account as a team until renewal point.

The objective here is to try and retain customers, and reduce churn whilst balancing that with the demands of the rest of the business.  For example, if a customer is at risk over a feature that needs to be built, that cost on engineering/product time needs to be balanced with the commercial attractiveness of the customer.  For that reason it is a good idea for a representative of the product team to also attend the review meetings, as required.

Having such a process in place ensures that we are being proactive about managing our churn risk. It also means that we gain good visibility of potential churn in advance. If there are commonalities to churn risk customers, that will also help with visibility and prioritisation of any work required - or of any other problems.

If you move an account to Green or Yellow from Amber or Red, close it. Make sure you close it AND add the label so we can see the history in the issue.

This project/process is a work in progress and can always be improved. If you have suggestions on how to improve it, please leave add your thoughts to [this issue](https://gitlab.com/gitlab-com/customer-success/tam/issues/80).

## Labels:

#### Health scores (https://gitlab.com/gitlab-com/customer-success/cs-metrics/issues/6)

Rough guide below on what the health of an account in a certain category looks like:

**Red**

* They’ve told us they are downgrading/cancelling.
* No communication.
* Limited or no access to executive sponsors.

**Amber**

* Low user adoption.
* Lack of response to us and/ or we haven’t done much discovery.
* Complains about the company (responsiveness, feature turnaround, missed priorities in direction).
* Loss of our GitLab champion.
* Original GitLab use-cases not achieved.
* At least two major releases behind.
* Frequently don't show up or are late to scheduled cadence calls.
* Raises more than 15 tickets a month.
* Customer was aquired and parent company uses competitor.
* Negative NPS scores.

**Yellow**

* We don’t have much info about how they are using GitLab.
* Don’t have the right contacts for each key person.
* Lack of clearly defined GitLab use-cases. 
* Only one primary stakeholder. 
* At least one major release behind. 
* Doesn't raise any tickets or contribute to any issues.
* Occasionally doesn't show up or are late to scheduled cadence calls.
* Raises more than 10 tickets a month.
* Neutral NPS scores.

**Green**

* Successful GitLab use-cases.
* Using the full GitLab DevOps lifecycle.
* Regular communication and positive relationship.
* Healthy user adoption rate.
* Regularly show interest and utilisation in new features.
* At least two successful EBR's every year.
* Always show up to regular cadence calls and communicates when they can't.
* They are interested in upgrading.
* Up to date on releases.
* Raises between 1 and 5 tickets a month and regularly contributes to issues.
* Regularly provides feedback on how to improve GitLab.
* Postive NPS scores.

#### Triage Labels for at risk (~red) accounts

* ~E&A
* ~E&U
* ~U&A
* ~U&U

See [Churn Classification](https://sixteenventures.com/churn-classification) for more info on these labels.

#### Workflow labels

* ~New

Every new issue will have the ~New label. Use the "Colours" board as your main view when viewing issues via an issue board. Once you have reviewed the issue, remove the ~New label.