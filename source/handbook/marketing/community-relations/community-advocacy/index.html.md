---
layout: markdown_page
title: "Community Advocacy"
---

## Finding the Community Advocates

- [**Community Advocacy Issue Tracker**](https://gitlab.com/gitlab-com/marketing/community-advocacy/general/issues); please use confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/messages/community-relations); please use the `#community-relations` chat channel for questions that don't seem appropriate to use the issue tracker for.

## On this page
{:.no_toc}

- TOC
{:toc}

----

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Community Advocate Resources

- Community Advocate Onboarding
  - [Onboarding](/handbook/marketing/community-relations/community-advocacy/onboarding/checklist/)
- Community Advocate Bootcamp
  - [Bootcamp](/handbook/marketing/community-relations/community-advocacy/onboarding/bootcamp/)

----

## Role of Community Advocacy

### Goal

The goal of community advocacy is to grow the number of active GitLab content contributors. We do this by increasing conversion in the [contributor journey](/handbook/journeys/#contributor-journey).

### Plan

1. Have discount codes that are easily distributed by team members
1. Send every major contributor a personalized gift
1. Host online sessions for content contributors
1. Start keeping track of our core contributors
1. Do the rest of the [contributor journey](/handbook/journeys/#contributor-journey)

### Vision

1. GitLab has 1000's of active content contributors (e.g. for blogs, meetups, presentations, etc.)
1. Being a core contributor is a very rewarding experience
1. There are 10's of active GitLab/[ConvDev](http://conversationaldevelopment.com/) meet-ups
1. 100's of talks per year given at conferences and meetups
1. Our most active content contributors come to our summits
1. 100's of people contribute content about GitLab every month
1. We use software that helps us to keep track of core contributors (can be forum, Highrise, software made for advocacy, or a custom Rails app)
1. There is a core contributors page organized per region with the same information as the [team page](/company/team/) and what they contributed, where they work (if they have a linkedin profile), and a button to sent them an email via a form.
1. We measure and optimize every step of the [contributor journey](/handbook/journeys/#contributor-journey)

### Respond to every community question about GitLab asked online

- This includes helping members of the community with _their_ questions, but also making sure that the community is heard and that the feedback from the community reaches the rest of the team at GitLab.
- Engage with the developer community in a way that is direct but friendly and authentic. Be able to carry the tone of the GitLab brand while also giving the proper answers or direction to members of the community.
- Help update the [social media guidelines](/handbook/marketing/social-media-guidelines/) and GitLab voice as new situations arise.
- Work with leadership to find a way to track and measure response time across all channels with the ideal response time being under 1 hour for all channels by the end of Q1, 2017.
- Explore different tools from Zendesk to Mentions to find a way to track all mentions of GitLab across the internet.
- Don’t be afraid of animated gifs and well-placed humor! We are not robots.
- Work within the GitLab process to help users report bugs, make feature requests, contact support, and provide feedback on the product.

#### Community response channels

The Community Advocates actively monitor and respond to the following set of channels.

In this overview:
- Those channels not marked as active need a response workflow to be put in place and are currently monitored on an occasional basis.
- Each channel has a link to the workflow in place to process responses

| CHANNEL | SOURCE | AUTOMATION | DESTINATION | ACTIVE? |
| - | - | - | - | - |
| [`@gitlab`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter.html) | Twitter mentions | Zendesk | Zendesk | ✓ |
| [`@movingtogitlab`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter.html)  | Twitter mentions | Zendesk | Tweetdeck | ✓ |
| [`@gitlabstatus`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter.html)  | Twitter mentions | Zendesk | Zendesk | ✓ |
| [Facebook](/handbook/marketing/community-relations/community-advocacy/workflows/facebook.html)  | Facebook page messages | Zapier | Zendesk | ✓ |
| [Hacker News](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews.html) | Hacker News mentions | Zapier | Zendesk and Slack: #hn-mentions | ✓ |
| [Education initiative](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource.html) | Education application form | Marketo | Salesforce and Zendesk | ✓ |
| [Open Source initiative](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource.html) | Open Source application form | Marketo | Salesforce and Zendesk | ✓ |
| [E-mail (merch@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail.html) | Shop contact | E-mail alias | Zendesk | ✓ |
| [E-mail (community@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail.html) | Handbook | E-mail alias | Zendesk | ✓ |
| [E-mail (movingtogitlab@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail.html) | #movingtogitlab campaign (deprecated) | E-mail alias | Zendesk | ✓ |
| [E-mail (education@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail.html) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (opensource@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail.html) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (personal inbox)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail.html) | E-mails to track as tickets | E-mail alias | Zendesk | ✓ |
| [GitLab blog](/handbook/marketing/community-relations/community-advocacy/workflows/blog.html) | Disqus comments | Zapier | Zendesk | ✓ |
| [DevOps Tools page](/handbook/marketing/community-relations/community-advocacy/workflows/devops-tools.html) | Disqus comments | Zapier | Zendesk | ✓ |
| [Speakers](/handbook/marketing/community-relations/evangelist-program/workflows/find-a-speaker.html) | Find-a-speaker form | Zapier | Zendesk | ✓ |
| [Combined mentions](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | Product Hunt, Hacker News, Reddit, YouTube, Quora | notify.ly | Slack: #mentions-of-gitlab | ✖ |
| [Documentation](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | Disqus comments | Zapier | Slack: #docs-comments | ✖ |
| [Reddit](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html)  | Reddit mentions | Zapier | Zendesk | ✖ |
| [Stack Exchange](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | Stack Exchange mentions | N/A | N/A | ✖ |
| [GitLab forum](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | forum.gitlab.com | N/A | N/A | ✖ |
| [IRC](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | IRC support | N/A | N/A | ✖ |
| [Gitter](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | Gitter support | N/A | N/A | ✖ |
| [YouTube](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | YouTube comments | N/A | N/A | ✖ |
| [Mailing list](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | GitLabHq Google Group (deprecated) | N/A | N/A | ✖ |
| [Quora](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | GitLab Quora topic | N/A | N/A | ✖ |
| [Wider community content](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html) | Blog post comments | N/A | N/A | ✖ |

## How we work

- [Community advocacy workflows](/handbook/marketing/community-relations/community-advocacy/workflows/)
- [Community advocacy guidelines](/handbook/marketing/community-relations/community-advocacy/guidelines/)

## Deliverable scheduling

* Team meetings are on Tuesday
* One on one's are on Thursday
* All deliverables are expected to be completed by Friday of the running week
  * Lots of time to complete
  * Enough time to review
  * Enough time for resolving potential problems
  * Small deliverables force small / fast iterations
* All deliverables are expected to be merged by Tuesday
* For every handbook update (that substantially change content/layout), please follow the [handbook guidelines](/handbook/handbook-usage/#handbook-guidelines)

### Release advocate duty

Every 22nd of the month we release a new version of GitLab. More often than not we get a spike in community mentions. To help deal with this we have dedicated release advocates that own the effort of responding to community mentions on/after a release.

Every month a different advocate has release advocate duty. It rotates on a monthly basis. If the release day takes place on weekend, one of the advocates is assigned to monitor the traffic and to process mentions.

The two channels that we see the biggest increases in are:

* The GitLab blog
* HackerNews

### Involving experts

While we're restructuring our handbook, this topic has now moved to the [Involving experts workflow section](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts.html).

#### Can you please respond to this?

You got a link to this because we'd like you to respond to the mentioned community comment. We want to make sure we give the best answer possible by connecting the wider community with our experts and expose you to more community feedback.

When responding to community mentions, you should check out the [social media guidelines](/handbook/marketing/social-media-guidelines/). Please answer in the social channel that the comment was originally posted in - discussing it internally via Slack makes it impossible for the community member to interact.

If you can't respond to the linked comment, that's OK, but please quickly let the person who pinged you know so they can ping someone else.

## Initiatives

### Education / OSS

While we're restructuring our handbook, this topic has now moved to the [Education/open Source workflow section](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource.html).

## Supporting community initiatives

When we see outstanding articles about GitLab from our wider community, we should acknowledge the author.

Please create an issue for every such initiative to track our effort of supporting the community member in the [Community Advocacy Issue Tracker](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/issues).

### Outreach email template

This is an email template for outreach that should be used to contact those users, thank them for their contributions and offer further support.

```
Hello NAME,

I'm reaching out to thank you for being such an incredible part of the GitLab community! Specifically, for taking the time to write about our product and help us grow our community.

We appreciate everyone's effort to spread the word about GitLab, especially when it's an individual initiative. This is something that we really like because it is the ultimate sign that we're on the right path with our vision.

We're so blessed to have such an awesome community, and we'd like to hear how can we thank you in the best possible way!

Is there something we can do to support you better? There are several things that we can think of:
1. Let us know if you want $100 of free swag from https://shop.gitlab.com/ so that we can send you a coupon.
1. Would you like us to cross-post this on our blog?
1. Is there any technical support we could offer you as you're working with GitLab?
1. Would you like to be introduced to GitLab developers so that you can discuss more advanced topics?
1. Would you be interested in doing webinars or blog posts together with us?

Finally, please consider adding yourself to the speaker's list if you're intersted in speaking about GitLab: https://about.gitlab.com/find-a-speaker.

I'm looking forward to hearing back from you, NAME. Could you please continue the conversation in the following issue LINK. If you have any other questions, feel free to include them in your reply. Thanks for promoting GitLab. Take care and have a great day!

Sincerely,
YOUR_NAME
```

##### Please check if they replied in the issue/email. If that's not the case for 1 week, please reach out again with the following template.

```
Hello NAME,

We hope you are fine, just checking in to see if you want to proceed with our offer. 

We always care about our community and we’d like to help you out with your contributions. 

Please let us know if you want to proceed further, we can’t wait to share some swag with you!

Regards,
NAME
```
If they don't respond after a week or two, make your decision and feel free to close the issue.


### Thank you swag

Always take care of GitLab contributors. Look for contributors and send some swag (stickers/T-shirt).

### Users speaking about GitLab

Keep updating the speaker list with outside contributors.

Users who tweet about their upcoming/previous talk about GitLab:

* Reach out to them and thank them
* Ask if they want some swag (usually stickers) for them or to share vith audience
  * Ask if they wants to be added to the [speakers list](/events/find-a-speaker/)

_NOTE:_ Don't ask for the private information on Twitter, collect user information via email.

## advocate-for-a-day

When community advocates aren't available, or we expect high traffic on social media (because of some major outage, or some significant announcement), we should try to recruit more Gitlabbers who would help us cover our social networks.

A good way to quickly organize these volunteers is using:
- [community-advocates](https://gitlab.slack.com/messages/community-advocates) Slack channel
- [Advocate for a day notes](https://docs.google.com/document/d/1giSLgYgEr5zb6ZtE2e69UZ0Dz30hD8CPH7BDRXdJ5GM/edit#) Google Doc

## Expertises

Every Community Advocate owns one or more of the processes that the CA team uses. These are called expertises.

* Swag, Supporting Community Individuals - [Djordje](https://gitlab.com/sumenkovic)
* Initiatives, Maintainer of [OSS project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program) - [Borivoje](https://gitlab.com/borivoje)

## Relevant Links

- [Social Media Guidelines](/handbook/marketing/social-media-guidelines/)
- [Support handbook](/handbook/support/)
