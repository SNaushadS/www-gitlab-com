---
layout: markdown_page
title: "Community advocacy workflows"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Community response workflows

- [Hacker News](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews.html)
- [Education and Open Source](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource.html)
- [Twitter](/handbook/marketing/community-relations/community-advocacy/workflows/twitter.html)
- [Website comments](/handbook/marketing/community-relations/community-advocacy/workflows/website-comments.html)
- [E-mail](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail.html)
- [Inactive workflows](/handbook/marketing/community-relations/community-advocacy/workflows/inactive.html)

## Other workflows

- [Involving experts](involving-experts.html)
- [Knowledge base](knowledge-base.html)
