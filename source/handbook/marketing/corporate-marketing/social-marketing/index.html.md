---
layout: markdown_page
title: "Social Marketing Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Social activities <a name="social activities"></a>
In addition to promoting corporate and field events, content marketing assets, company updates, etc., we also undertake some activities *just* for our social communities. These help us communicate with and nurture our social communities, so we're not always treating social as a way to extract value or information from our followers. You wouldn't stay in a relationship that one-sided in real life, so why would you with a brand? 

Here are some examples of native social events and social activities. 
You can [open an issue in the marketing project](https://gitlab.com/gitlab-com/marketing/general/issues) and mention @evhoffmann if you'd like to propose a social activity.
* Twitter polls 
  - [Topics](https://gitlab.com/gitlab-com/marketing/general/issues/1918)
  - Twitter [chats brainstorm](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/49)
  - Miscommunication [example and blog post](https://about.gitlab.com/2018/03/20/managers-more-optimistic-than-developers/)
  - Burnout [example](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/18)
* Live tweeting
  - [Series D live tweeting](https://gitlab.com/gitlab-com/marketing/general/issues/3116)
* Employee social advocacy [guidelines and education](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/45)
* Short-term banner art
  - [Seasonal & event banners examples](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/25)
  - [Year-end planning](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/125)
* [Glassdoor engagement and content updates](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/53)
* GitLab Medium publication and engagement
  - [GitLab Magazine](https://medium.com/gitlab-magazine)
  - Updated with [unique content](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/11528#note_110815003) and [imported content](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/89)
* YouTube maintenance
  - [YouTube Community Tab](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/33)
  - [Curated playlists](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/40)
* Live events (i.e. Facebook or YouTube live)
* Short-term social takeover by a team member or influencer
* Influencers WIP interaction [plan](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/47)
* Facebook group 
  - [Brainstorm](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/35)

## Social Takeover Notes <a name="social takeover notes"></a>

So you've been nominated for a short-term takeover of GitLab's social channels — here's a quick guide with everything you need to know.

**Format:**
* Create [tracked links](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) for every link back to about.gitlab.com (e.g., all blog posts, landing pages, etc.)
* Publish and schedule in Sprout, using the “shorten links” button
* Choose a tag for every post that goes out (there is a dropdown)
* Preview posts in Sprout to make sure spacing, image, etc. looks fine on desktop and mobile

**Timing:**
* **Check the schedule in Sprout.**
* Do not publish more than 1-2 times per day on Facebook & LinkedIn (studies show > 2 posts per day on brand pages on these platforms starts to feel spammy).
* Do not publish tweets within 30 minutes of each other. Retweets of other stuff within that time is fine.
* Especially if someone reacts to an article that you tweeted with a substantive comment, question, or conversation starter, that is a good one to retweet promptly, because it could spur additional engagement.
* If you’re debating between posting early and posting late, post early (i.e., 4 am Pacific). We have a sizable audience in Europe, so it’s best to resist posting after 4 pm PT (unless it’s a retweet, or a post that is newsworthy and published late)

**Content:**
* Newly published posts
* Retweet of substantive, high quality tweets, especially from team members
* Archive posts
   * If you feel the day has been light on content, feel free to check out [sample copy & assets here](https://docs.google.com/spreadsheets/d/1_RdHsIflAdLjvZKzCzMCa-xBgwZYoZgGBJr0E-6Djcs/edit?usp=sharing).
* Can also fill some space in the afternoon with a job posting (“jobs of the week” are in the [company call agenda](https://docs.google.com/document/d/1JiLWsTOm0yprPVIW9W-hM4iUsRxkBt_1bpm3VXV4Muc/edit) doc)
* Have fun with it! Be liberal with emojis.

**Engagement:**
* In addition to retweeting described above, please perform a search on `#gitlab` and `gitlab` a few times per day (can be within Sprout or twitter) and favorite positive mentions
* Any particularly funny or complimentary posts, you can comment on with a gif, :blush:, “Thanks! We’re so glad you like it, let us know how we can help!” etc.
* Be creative but refer back to these guidelines for the [GitLab voice](https://about.gitlab.com/handbook/marketing/social-media-guidelines/#gitlab-voice)

**Signing off**
* If you know there is going to be a long (1+ days) delay between when you need to sign off and when the next person takes over (i.e., for Summit travel), I recommend scheduling a tweet saying something like “The team is heading to beautiful Cape Town for our xth Summit! See you on the other side :airplane:” This helps prime people for the disruption of regular content.

## Requesting Social Promotion <a name="requesting event promotion"></a>

#### Open a separate issue for social
- Some of the following is written specifically for field and corporate events; please follow the process and use the same issue template even if you are not requesting social for an event.
- Open a separate issue for social in the [marketing general project](https://gitlab.com/gitlab-com/marketing/general/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) using the `event-social` label only *once the event has been approved* and *a landing page has been created* (i.e., try to only open an issue once it is ready for action.)
- For an event to get promoted on social, there must be a dedicated social issue. Mentioning social in the meta issue is insufficient. 
- If the need is urgent (i.e., the event is happening *now*), Slack Emily vH. She reserves the right not to publish on social.
- Luke will create social images if time permits - *at least* one week before desired publication is required for unique social images to be produced. If Luke has already been engaged for booth art, he may be able to turn them around on a shorter timeline, but this is at his discretion. 

#### What to expect
- Major corporate events will be promoted via organic social on GitLab's social channels. Emily vH will manage this.
- Field events with landing pages on about.gitlab.com will be promoted via paid social, so that we do not saturate our audience, and so that we can use the more advanced targeting that paid social allows. The Digital Marketing Specialist (Starts Nov. 5 - interim LJ) will manage this.
  - Don't worry about the divide between paid and organic - we will determine the best fit for the event and coordinate among ourselves.
- Note: the below are general suggestions based on best practices; the social media manager reserves the right to dial up or down the number of posts based on the event type and existing schedule.
- Once images are finalized, Emily vH will schedule social within 2 days.
- Tweets will go out from the corporate account: 1 week or more before event;  1 day before event; on the day of, with venue pictures if possible (see below)
- The corporate account will also selectively retweet relevant tweets from GitLabbers attending or speaking at the event.
- LinkedIn and Facebook posts will go out: one week or more before the event; a few days before the event. 

#### While at the Event:
- Take pictures of booth and/or team members at booth and send them over Slack to Emily vH. Be sure to include any updates to the booth location (for instance, if the venue is confusing, you can say "near the north entrance", etc.)
- Emily vH will aim to post them ASAP. If you are Slacking her outside of normal hours (for instance, for an event in EMEA), please have someone at the booth tweet from their own account using the event hashtag, and Emily vH will retweet from the corporate account when she starts the next day as a follow-up tweet.

## Giveaways <a name="giveaways"></a>

We use giveaways to encourage and thank our community for participating in marketing events such as surveys, user-generate-content campaigns, social programs, and more.

### Giveaways Process <a name="giveawaysprocess"></a>

**Pre-giveaway**
1. Create an issue and tag Emily von Hoffmann (@evhoffmann) to determine the
rules of engagement and Emily Kyle (@emily) for prizes.
2. Create and publish an [Official Sweepstakes Rules page](#officialrules)

**Post-giveaway**
1. Winners must sign an Affidavit of Eligibility & Liability, Indemnity, and Publicity Release. Use the "Affidavit of Eligibility - Sweepstakes" template found on the google drive.
2. Announce the winners

### Social Support & Logistics for Giveaways

#### Creating the Campaign

- Set a launch date
- Ask for social image(s) with text (if organic posts only) explaining the offer/ask
- Set an initial deadline for submissions, so you can have multiple pushes at interval & ramp up energy  
- Finalize the delivery method: form vs. tweets vs. retweets, depending on the goals of the campaign
    - Pros of a form: Neat, uniform, easy for us to keep track of, no downsides of low engagement (i.e., responses not visible)
    - Pros of asking for submissions via Twitter: we could more easily RT cool responses, get more out of a hashtag, etc.
    - Pros of asking for RTs in exchange for swag: very little backend to do on social afterwards, except to announce the winners of swag
- Finalize the ask, making sure it's extremely clear what you want to happen (`Share your GitLab story!` `Tell us your favorite thing you made with GitLab` `tell us a time GitLab helped you out of a tight spot`)
    - Make sure the ask can be intuitively communicated via whichever delivery method you're using, i.e., the tweet doesn't need to explain everything if you're pointing to a form or blog post. If you're not pointing to anything, make sure the tweet plus possible image text must make sense by themselves. Use threads for more space!

#### Pre-launch

- Finalize the timeline for when the reminders/follow-ups will go out, add to social schedule and leave some space around them to RT/engage with responses
- Finalize copy for all pushes
- If swag is involved, create a google sheet with swag codes from Emily Kyle
- Finalize hashtag
- Ask community advocates to review all copy (tweets, form, blog post) and adjust according to their suggestions
- Make sure the community advocates are aware of the campaign timeline/day-of
- Designate a social point person to be "on duty" for the day-of and one person who can serve as backup
- Let the broader GitLab team know that the social campaign is upcoming and ask for their support

#### Day of giveaway
- If you have entries for the giveaway in a spreadsheet, use [random.org](https://www.random.org/) to generate a random number. Match the number to the corresponding row in your spreadsheet to identify the winner. **Never enter email addresses or personal information of participants into a third-party site or system we do not control.**
- Try to schedule first push or ask a team member to tweet the first announcement early (ex: around 4 am PT) to try to have some overlap with all our timezones
- If you're asking for RTs in exchange for swag, make sure there's a clearly communicated cut-off to indicate that the giveaway will not stretch into perpetuity. One day-long is probably the longest you want a giveaway to stretch, or you can limit to number of items.
- Plan to engage live with people
   - If your promise was to give away one hoodie per 25 RTs, do it promptly after that milestone is crossed. It adds to the excitement and will get more people involved
- Announce each giveaway and use handles whenever possible, tell them to check their DMs
- DM the swag codes or whatever the item is
- In your copy, directly address the person/people like you are chatting with them irl
- RT and use gifs with abandon but also judgment

#### After the Giveaway
- Thank everyone promptly, internal & external
- Write in the logistics issue of any snags that came up or anything that could've gone better
- Amend hb as necessary for next time

### How to Create an Official Sweepstakes Rules Page <a name="officialrules"></a>

1. Create a new folder in `/source/sweepstakes/` in the www-gitlab-com project. Name the folder the same as the giveaway `/source/sweepstakes/name-of-giveaway`
2. Add an index.html.md file to the `/name-of-giveaway/` folder
3. Add the content of [this template](https://gitlab.com/gitlab-com/marketing/general/blob/0252a95b6b3b5cd87d537dabf3d1675023f1d07d/sweepstakes-official-rules-template.md) to the `index.html.md` file.
4. Replace all bold text with relevant information.
5. Create merge request and publish.

## Social Channels and Audience Segmentation <a name="social channels and audience segmentation"></a>

You can find a list of evergreen content assets their primary channel for promotion [here](https://docs.google.com/spreadsheets/d/1_RdHsIflAdLjvZKzCzMCa-xBgwZYoZgGBJr0E-6Djcs/edit?usp=sharing).

### Twitter <a name="twitter"></a>

**Content/Execution**
* Broad audience - all our content gets shared here, but tone should still appeal to devs.
* 5 or more tweets per day, plus retweets
  * Schedule between 12 am - 5 pm PT (A large part of our audience is in EMEA, and we find an increase in organic impressions and engagement when we schedule tweets for their morning and workday)
  * At least 30 mins apart
* Aim for variety & roughly even mix of main topics: Git, CI/CD, collaboration, DevOps, employer branding, product-specific, community articles & appreciation
* Check mentions at least once per day, pull out articles to share with the team and favorite positive mentions.
*  Keep a list of our most engaged followers; keep these in mind for opportunities outside of social--blogging, live streamed interview etc.

**Goals**
* 15 tweets per day
* 2-4 videos published natively per month
* 5 lead-generating content items from the backlog published per month
* Consider [takeovers.](http://www.telegraph.co.uk/news/2017/07/12/work-experience-boy-takes-southern-rails-twitter-account-things/) Would need to develop an instructions kit & think about how/who to pilot.

### Facebook

**Content/Execution**
- Developer/community- and thought-leadership focused
- 1-2 facebook posts per day
- At least 2 hours apart
- Live events are more company/culture focused
    - talks by or AMAs with People Ops, etc.

**Goals**
- Consider streaming a webcast on Facebook Live (a simple toggle switch in Zoom allows this) and compare the performance to YouTube live streaming
- When the speaker cameras are enabled in Zoom webinars, natively publish clips of webcasts for post-promotion.

### LinkedIn <a name="linkedin"></a>
Sometimes, posts that GitLabbers propose for our blog may be a better fit for native publishing on LinkedIn. This is not a negative, it's usually due to the content team's strategic priorities at the time. Rebecca and Emily vH will recommend that you publish on your own LinkedIn, and if you agree, Emily vH will help you finalize the post and socialize it internally for best results. 

**Content/Execution**
- IT buyer/manager focus
- 3-5 posts per week
  - Non-tutorial original blog posts
  - Press releases/product announcements
- Posts around particular features, releases, company culture, people ops new practices, problem/solution or personal growth structured posts; include links back to homepage

**Goals**
* Identify key LinkedIn groups to join/share content in
* Native posts on LinkedIn
   * Encourage targeted employees to do so; think of incentive/framing as professional development that we will help edit.
* Curate and share articles from other publishers on remote work/culture that we want to elevate

### Medium <a name="medium"></a>
GitLab has a [Medium publication](https://medium.com/gitlab-magazine), and all GitLabbers may be added as writers! To be added as a writer to the publication, [import](https://help.medium.com/hc/en-us/articles/214550207-Import-post) a blog post that you authored on about.gitlab.com/blog to your personal Medium account, and submit it to the GitLab publication. Emily vH will approve you as a writer and help finalize the post before publishing. 

**Content/Execution**
* Brand and thought leadership-focused
* Favorite articles about GitLab
* Aim to post one article to Medium every two weeks
* Cross-post our original thought-leadership posts to Medium, linking back to blog
  - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste.

### YouTube <a name="youtube"></a>

**Content/Execution**
- Developer/community-focused
- Live events are more tech focused
  - Group Conversations, pick your brain meetings, demos, brainstorms, kickoffs

### Google+ <a name="google+"></a>

**Content/Execution**
- Re-post older articles from the archive periodically that we want to continue giving a boost
- Aim for at least one post per week




