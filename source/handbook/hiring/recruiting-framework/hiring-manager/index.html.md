---
layout: markdown_page
title: "Recruiting Process - Hiring Manager Tasks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiting Process Framework - Hiring Manager Tasks
{: #framework-hm}

### Step 1/HM: Identifying hiring need

1. Determine the purpose of this role
1. Is it a new or existing role?
1. Create or adjust a job description that outlines the positions main responsibilities and basic qualifications
1. See the [vacancy creation process](https://about.gitlab.com/handbook/hiring/vacancies/#vacancy-creation-process) in the handbook.

### Step 2/HM: Create vacancy in Greenhouse

Create the vacancy in Greenhouse following the [instructions in the handbook](https://about.gitlab.com/handbook/hiring/vacancies/#open-the-vacancy-in-greenhouse) or by watching the [internal training video](https://drive.google.com/file/d/1S2d8XE6Ri6U4fAz6jh3R9DC593YhTcjF/view?usp=sharing).

### Step 12/HM: Complete feedback in Greenhouse/next steps

Once the first-round interview with the hiring manager is completed, the hiring manager will submit feedback via the designated scorecard in Greenhouse. If the candidate is still considered top talent, the hiring manager will then tag the recruiter in Greenhouse to confirm next steps in the hiring process. The hiring manager will have designated areas of focus for each member of the interview team communicating to them via Slack channel or email.  

**Template Resource:**

> Thank you for making yourself available to be part of the interview team for our [TITLE] role. It is important that your calendar is up to date to ensure a smooth scheduling process. If you have not updated your calendar, please do so now! A few things you need to know before interviewing candidates.
>
> Key things we are looking for specific to the role:
> > a. Example 1
> >
> > b. Example 2
> >
> > c. Example 3
>
> Individual focus areas during your interview (attached reference if needed):
> > a. [recruiter to insert name of Interviewer 1] - [focus area]
> >
> > b. [recruiter to insert name of Interviewer 2] - [focus area]
> >
> > c. [recruiter to insert name of Interviewer 3] - [focus area]
>
> **Please** complete your interview notes in Greenhouse within 1 business day of your interview. If you have any trouble please contact your recruiter or coordinator.
>
> Additionally, your referrals are important to us! If you have any referrals for this role (or other roles that are currently posted at GitLab) please submit them following the [guidelines in the handbook](https://about.gitlab.com/handbook/hiring/greenhouse/#making-a-referral).

**Interview Resource**

You can review [sample interview questions for the interview team](https://docs.google.com/document/d/1Eb7GUUH0b9wzf1WxuyKUYhpjP774VbO5sQWtiDZX-PM/edit) based around specific focus areas. 

### Step 15/HM: Hiring team to complete feedback in Greenhouse

Once second-round interviews with the interview team are completed, the interview team will submit feedback via the designated scorecard in Greenhouse within 1 business day of their interviews.

### Step 19/HM: Complete references

Once the recruiter has notified the hiring manager of the references sent by the candidate, the hiring manager will complete references to establish the candidate's strengths and opportunities, to set them up for success once joining GitLab. At least 2 references must be completed, and one of them must be a manager or supervisor. For more information on how to conduct reference checks and why it is important, please watch [the reference check training video](https://about.gitlab.com/handbook/hiring/interviewing/#reference-check-process) in the handbook.

