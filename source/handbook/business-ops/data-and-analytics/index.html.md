---
layout: markdown_page
title: "Data & Analytics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Quick Links:
Primary project: [https://gitlab.com/meltano/analytics/](https://gitlab.com/meltano/analytics/)
Looker project: [https://gitlab.com/meltano/looker/](https://gitlab.com/meltano/looker/)
dbt docs for CloudSQL dbt project: [https://meltano.gitlab.io/analytics/dbt/cloudsql/#!/overview](https://meltano.gitlab.io/analytics/dbt/cloudsql/#!/overview)
dbt docs for Snowflake dbt project: [https://meltano.gitlab.io/analytics/dbt/snowflake/#!/overview](https://meltano.gitlab.io/analytics/dbt/snowflake/#!/overview)

## The Data Analysis Process

Analysis usually begins with a question. A stakeholder will ask a question of the data and analytics team by creating an issue in the [Looker project](https://gitlab.com/meltano/looker/) using the appropriate template. The analyst assigned to the project may schedule a discussion with the stakeholder(s) to further understand the needs of the analysis. This meeting will allow for analysts to understand the overall goals of the analysis, not just the singular question being asked, and should be recorded. Analysts looking for some place to start the discussion can start by asking: 
* How can your favorite reports be improved?
* How do you use this data to make decisions?
* What decisions do you make and what information will help you to make them quicker/better?

An analyst will then update the issue to reflect their understanding of the project at hand. This may mean turning an existing issue into a meta issue or an epic. Stakeholders are encouraged to engage on the appropriate issues. The issue then becomes the SSOT for the status of the project, indicating the milestone to which its been assigned and the analyst working on it, among other things. Barring any confidentiality concerns, the issue is also where the final project will be delivered. On delivery, the data and analytics manager will be cc'ed where s/he will provide feedback and/or request changes. When satisfied, s/he will close the issue. If the stakeholder would like to request a change after the issue has been closed, s/he should create a new issue and link to the closed issue. 

The Data and Analytics team can be found in the #analytics channel on slack.

## Getting Work Done

The data team currently works in two-week intervals, called milestones. Milestones may be three weeks long if they cover a major holiday or if the majority of the team is on vacation. As work is assigned to a person and a milestone, it gets a weight assigned to it. 

### Notes on Pointing process

* We use Fibonacci numbers for pointing, e.g. 1, 2, 3, 5, 8, etc. 
* If something is greater than 8 points, it probably should be multiple issues. 
* We size and point issues as a group. Size is about the complexity of the problem and not its complexity in relation to whom is expected to complete the task.
* Effective pointing requires more fleshed out issues, but that requirement shouldn't keep people from creating issues.
* Rules of thumb:
   * `NULL` points belongs to a meta issue, where the sub issues get points.
   * 0 points is a not-complex unit of work and is different from `NULL`.
   * If we start working on an issue that we decide to stop working on, the issue keeps its point value. Just use a label to indicate that work is not being done on it.

## Tips and Tricks about working in the [Analytics Project](https://gitlab.com/meltano/analytics/)

*Ideally*, your workflow should be as follows:
1. Create an issue
1. Open an MR from the issue using the "Create merge request" button. This automatically creates a unique branch based on the issue name. This marks the issue for closure once the MR is merged.
1. Push your work to the branch
1. Have it reviewed by a peer
1. Remove `WIP:` and assign to the project's maintainer once it's ready for merging and further review.

Other tips:
* If, for some reason, the merge request is closed but not Merged, you have to run the review stop job manually. Closing the MR will not trigger it. 
* If you're on a review instance of the database and you need to test a change to the SFDC snapshot, truncate the table first.


## Extract and Load

The data team is the first customer of [Meltano](/handbook/meltano/). Wherever possible, we rely on Meltano for extractors and loaders.  

### Adding new Data Sources

Process for adding a new data source:
* Create a new issue in the Analytics repo requesting for the data source to be added:
  * Document what tables and fields are required
  * Document the questions that this data will help answer
* Create an issue in the [Security project](https://gitlab.com/gitlab-com/gl-security/engineering/issues/) and cross-link to the Analytics issue.
  * Tag the Security team `gitlab-com/gl-security`


### Using SheetLoad

SheetLoad is the process by which a GoogleSheet can be ingested into the data warehouse. This is not an ideal solution to get data into the warehouse, but may be the appropriate solution at times. 

How to use SheetLoad

1. Add file to [SheetLoad Google Drive Folder](https://drive.google.com/drive/u/0/folders/1F5jKClNEsQstngbrh3UYVzoHAqPTf-l0) with appropriate naming convention, described in detail below
2. Share the sheet with with the SheetLoader runner => Doc with email ([GitLab internal](https://docs.google.com/document/d/1m8kky3DPv2yvH63W4NDYFURrhUwRiMKHI-himxn1r7k/edit))
3. Add the full file name to the [`extract-ci.yml`](https://gitlab.com/meltano/analytics/blob/master/extract/extract-ci.yml#L90) file
4. Create dbt base models
5. Add to [data quality test](https://gitlab.com/meltano/analytics/tree/master/transform/cloudsql-dbt/tests) that helps ensure these files are updated monthly. 

### Warehouse Access

To gain access to the data warehouse: 
* Create an issue in the [access requests project](https://gitlab.com/gitlab-com/access-requests) documenting the level of access required. 
* If access to raw tables is required you will have to sign an additional legal agreement.
* Do not request a shared account - each account must be tied to a user.

## Orchestration

We are in the process of moving from GitLab CI to Airflow.

## Database

We are in the process of moving from CloudSQL to Snowflake.

## Transformation- dbt

* Getting started with dbt
* How to use dbt docs

### Tips and Tricks about Working with dbt
* The goal of a (final) `_xf` dbt model should be a `BEAM*` table, which means it follows the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business. 
* Model names should be as obvious as possible and should use full words where possible, e.g. `accounts` instead of `accts`.
* Documenting and testing new data models is a part of the process of creating them. A new dbt model is not complete without tests and documentation.  
* Definitions to know
   * `source table` - (can also be called `raw table`) table coming directly from data source as configured by the manifest. It is stored directly in a schema that indicates its original data source, e.g. `sfdc`
   * `base models`- the only dbt models that reference the source table; base models have minimal transformational logic (usually limited to filtering out rows with data integrity issues or actively flagged not for analysis and renaming columns for easier analysis); can be found in `analytics` schema; is used in `ref` statements by `end-user models`
   * `end-user models` - dbt models used for analysis. The final version of a model will likely be indicated with an `_xf` suffix when it’s goal is to be a `BEAM*` table. It should follow the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business.


## Visualization- Looker

Looker is GitLab's data visualization tool. Many modern data visualization tools require analysts to write SQL queries; Looker's unique advantage lies in their modeling layer that allows non-technical end users to build their own data analysis in a drag and drop user interface. While this means that the initial configuration (e.g., setting up a new data source) takes longer than just querying a table would be, once the initial configuration is done, you have a new data set- a Looker explore- available for all users to take advantage of. The data team aims for Looker to be the SSOT for all of GitLab. 

### Getting Looker Access
To get initial Looker Access, please create a new access issue following Security's procedures in [this project](https://gitlab.com/gitlab-com/access-requests). There are multiple levels of user access: View-Only, Explorer, and Developer. View-only users have access to consume all existing visualizations and reporting. Explorers have the ability to manipulate Looker explores (data sets) to build their own looks and dashboards (analyses). Developers have the ability to edit LookML, Looker's proprietary modeling language. 

### Getting Started with Looker- A special note for users coming from Redash
Users coming from Redash or another query-based data visualization tool, especially those with a strong familiarity with SQL, may find themselves uniquely frustrated at how long it can take to answer a "simple" question when doing so aims to take advantage of new data. Any new data sources need to be brought into the data warehouse, modeled in dbt following the team's [dbt coding conventions](https://gitlab.com/meltano/analytics/blob/master/README.md#dbt), modeled in a LookML view, and added to a new or existing explore before an analysis can be built on top of it. While this initial configuration might seem like a bit of a *slog*, it moved all of the analyses configuration to be an up-front responsibility, making the explore you build usable not just for you, but for future users with related questions too. 

* Using Looker
* Developing in Looker

### Tips and Tricks about Working in Looker
* Confused about what you might be looking at? Start by understanding our [Operating Metrics](/handbook/finance/operating-metrics/).
* When making a change to an existing `_xf` view, be sure to limit explores already referencing that view with sets, so as not to accidentally clutter existing explores with new/irrelevant data. 
* If building a dashboard with historical data, it should be for historical data. It should not include current data. For example, a dashboard that looks at the last year, should not include the present month. 
* When adding a description to a value that has both a description and an abbreviation, be sure to include both in the description, e.g. `Technical Account Manager` should be `Technical Account Manager (TAM)`. This is to make it easier for users to search for fields within Looker and avoids ambiguity.
