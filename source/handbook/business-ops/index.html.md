---
layout: markdown_page
title: "Business Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Purpose
Overview of systems, workflows and processes for the three functional groups - [Sales](/handbook/sales), [Marketing](/handbook/marketing) and [Customer Success](/handbook/customer-success/) - that work closely together. This section is a singular reference point for operational management of the database and where the most up-to-date workflows, routing, rules and definition sets are managed.  Tracking all tools, license allocation, admins and contracts for the combined Go-To-Market tech stack. Tips, tricks and how to's for the various applications and external resources that will help user be more productive in the business systems.

## Data Integrity Policy
Data isn't perfect, processes change, people make mistakes. We recognize that and make sure that when we identify an opportunity to be more correct we are moving quickly to resolution.

Our Data Integrity Process [link pending] is meant to capture and document these improvements as they're discovered, and to ensure that communication is made to impacted parties in a timely manner.


## What is the difference between Business Ops, Sales Ops, Marketing Ops, and Marketing Programs?
<table>
  <tr>
    <th>Business Operations</th>
    <th>Sales Operations</th>
    <th>Marketing Operations</th>
    <th>Marketing Programs</th>
  </tr>
  <tr>
    <td>Answering the question: “How do we build go-to-market infrastructure to support company growth and pivots?”</td>
    <td>Quota assignments</td>
    <td>Day-to-day management, maintenance and integrations in the entire tech stack</td>
    <td>Day-to-day program and marketer support, including landing pages, Marketo program and Salesforce campaign management</td>
  </tr>
  <tr>
    <td>Strategic analytics and data management</td>
    <td>Field &amp; RD Forecasting</td>
    <td>Evaluate, purchase and implement new technologies into GitLab tech stack</td>
    <td>Development and growth of webcast program</td>
  </tr>
  <tr>
    <td>Cross-functional process automation and optimization</td>
    <td>Sales Compensation</td>
    <td>Lead management & Data enrichment</td>
    <td>Improve and develop newsletter program</td>
  </tr>
  <tr>
    <td>New business technology review</td>
    <td>Sales admin activities</td>
    <td>Sales/Marketing SLAs</td>
    <td>Grow the opt-in communication channel</td>
  </tr>
  <tr>
    <td>Holistic business systems administration, development, integration, architecture, and planning around the entire GTM tool stack.</td>
    <td>Ad hoc analysis for CRO</td>
    <td>Marketing process design</td>
    <td>Support Field Marketing with conference, event & VIP invitation, reminder and follow-up</td>
  </tr>
  <tr>
    <td>Prioritization of business operations issues</td>
    <td>Sales Process Training/Documentation</td>
    <td>Marketing & Sales process training and documentation</td>
    <td></td>
  </tr>
  <tr>
    <td>Salesforce administration, development, architecture, customizations, and  technical documentation</td>
    <td>Sales Enablement functions</td>
    <td>Prioritization of Marketing-related business operations issues</td>
    <td></td>
  </tr>
  <tr>
    <td>Customer portal integrations</td>
    <td>Sales training on process flows</td>
    <td>Marketing tech stack implementations, development and integrations</td>
    <td></td>
  </tr>
  <tr>
    <td>Integration processes with license app and signups</td>
    <td>Handbook documentation</td>
    <td>Salesforce administration</td>
    <td></td>
  </tr>
  <tr>
    <td>Reporting infrastructure</td>
    <td>Reporting and business analysis related to the sales function</td>
    <td>New technology review, implementation, customization, documentation and administration</td>
    <td></td>
  </tr>
  <tr>
    <td>Reporting infrastructure</td>
    <td>Prioritization of Sales-related business operations issues.<br>This means consolidating all of the requests from sales into a single funnel with prioritization.</td>
    <td></td>
    <td></td>
  </tr>
</table>



## Reaching the Teams (internally)
- **Public Issue Tracker**: A short list of the main, public issue trackers. For respective teams, there may be additional resources. Please use confidential issues for topics that should only be visible to team members at GitLab
    - [Sales](https://gitlab.com/gitlab-com/sales/issues/) - general sales related needs & issues
    - [Salesforce](https://gitlab.com/gitlab-com/salesforce/issues) - Salesforce specific needs, issues and questions
    - [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) - All issues related to website, product, design, events, webcasts, lead routing, social media and community relations
    - [Outreach-io](https://gitlab.com/gitlab-com/outreach/issues) - Outreach.io specific needs, issues and questions
    - [Customer Success SA Service Desk](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues) - your go to place for pre-sales, post-sales, customer training and professional services
    - [Business Operations](https://gitlab.com/gitlab-com/business-ops/Business-Operations) - all issues related to tool management, integrations, database
- **Email**: Please use the "Email, Slack, and GitLab Groups and Aliases" document for the appropriate team alias.
- **Slack**: A short list of the primary Slack channels used by these respective teams
    - `#sales`
    - `#marketing`
    - `#solutions_architect`
    - `#bdr-team`
    - `#sdrs_and_bdrs`
    - `#sfdc-users`
    - `#outreach`
    - `#lead-questions`
    - `#marketing-programs`

## Tech Stack

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Applications & Tools</th>
    <th class="tg-031e">Who Should Have Access</th>
    <th class="tg-yw4l">Whom to Contact w/Questions</th>
    <th class="tg-yw4l">Admin(s)</th>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#avalara">Avalara</a></strong></td>
    <td>Finance Team</td>
    <td><strong>Wilson Lau (primary)</strong><br>Art Nasser</td>
    <td>Wilson Lau<br>Art Nasser</td>
  </tr>
   <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#bizible">Bizible</a></strong></td>
    <td>Marketing OPS<br>Online Growth<br>Marketing Programs</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
   <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#by-appointment-only-bao">By Appointment Only (BAO)</a></strong></td>
    <td>Online Marketing<br>SDR Team Lead</td>
    <td><strong>Jeffrey Broussard</strong></td>
    <td>Jeffrey Broussard<br>JJ Cordz</td>
  </tr>
     <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#chorus">Chorus</a></strong></td>
    <td><strong>Recorders:</strong><br>US East Sales <br>US West Sales <br>Solutions Architects<br>US SDR Team <br>US BDR Team<br><br><strong>Listeners:</strong><br>Everyone Else</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino<br>Mike Snow</td>
  </tr>
   <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#carta">Carta</a></strong></td>
    <td>Access via GitLab credentials</td>
    <td>Paul Machle</td>
    <td>Paul Machle</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#clearbit">Clearbit</a></strong></td>
    <td>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>JJ Cordz (primary)</strong><br>Francis Aquino</td>
    <td>JJ Cordz<br>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#cloudextend-google-drive-for-netsuite">CloudExtend: Google Drive for NetSuite</a></strong></td>
    <td>Finance Team</td>
    <td>Art Nasser</td>
    <td>Art Nasser</td>
  </tr>
    <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#cookiebot">Cookiebot</a></strong></td>
    <td>Marketing OPS</td>
    <td>JJ Cordz</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#discoverorg">DiscoverOrg</a></strong></td>
    <td>Mid-Market AEs<br>Outbound SDR</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>Disqus</strong></td>
    <td>Varies</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#dogood">DoGood</a></strong></td>
    <td>Online Marketing<br>SDR Team Lead</td>
    <td><strong>Jeffrey Broussard</strong></td>
    <td>Jeffrey Broussard<br>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>Drift</strong></td>
    <td>Inbound BDR</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>Eventbrite</strong></td>
    <td>Field Marketing<br>Content Marketing<br>Marketing Programs<br>Marketing OPS</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
   <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#expensify">Expensify</a></strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>Wilson Lau (primary)</strong><br>Art Nasser</td>
    <td>Wilson Lau<br>Art Nasser</td>
  </tr>
  <tr>
    <td><strong>FunnelCake</strong></td>
    <td>Marketing OPS<br>Online Marketing</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
    <tr>
    <td><strong>Google Adwords</strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Product Marketing<br>Content Marketing<br>Marketing Programs</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#google-analytics">Google Analytics</a></strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Product Marketing</td>
    <td><strong>LJ Banks</strong><br>JJ Cordz</td>
    <td>JJ Cordz</td>
  </tr>
    <tr>
    <td><strong>Google Search Console</strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Production<br></td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz<br>Andrew Newdigate</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#google-tag-manager">Google Tag Manager</a></strong></td>
    <td>Online Marketing</td>
    <td>&nbsp;</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>GovWin IQ</strong></td>
    <td>Sales - Federal</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#leandata">LeanData</a></strong></td>
    <td>Marketing OPS<br>Sales OPS</td>
    <td><strong>JJ Cordz (primary)</strong><br>Francis Aquino</td>
    <td>JJ Cordz<br>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>LicenseApp</strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>Francis Aquino</strong></td>
    <td></td>
  </tr>
  <tr>
    <td><strong>LinkedIn Sales Navigator</strong></td>
    <td>Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
    <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#looker">Looker</a></strong></td>
    <td>All GitLab Employees<br>Investors</td>
    <td><strong>Taylor Murphy</strong><br>Emilie Burke</td>
    <td>Taylor Murphy</td>
  </tr>
  <tr>
    <td><strong>MailChimp</strong></td>
    <td>Marketing OPS<br>UX Team<br>Product Marketing<br>Marketing Programs</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#marketo">Marketo</a></strong></td>
    <td>Marketing OPS<br>Marketing Programs<br>Online Marketing<br>Content Marketing<br>Field Marketing</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#moz-pro">Moz Pro</a></strong></td>
    <td>Online Marketing<br>Marketing OPS</td>
    <td><strong>LJ Banks</strong><br>JJ Cordz (for access)</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#netsuite">NetSuite</a></strong></td>
    <td>Finance Team</td>
    <td><strong>Art Nasser (primary)</strong><br>Wilson Lau</td>
    <td>Art Nasser<br>Wilson Lau</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#outreachio">Outreach.io</a></strong></td>
    <td>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>JJ Cordz (primary)</strong><br>Francis Aquino</td>
    <td>JJ Cordz<br>Francis Aquino<br>Michael Snow</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#OwnBackup">OwnBackup</a></strong></td>
    <td>Business Operations</td>
    <td><strong>Michael Snow (primary)</strong><br>Francis Aquino (secondary)</td>
    <td>Michael Snow</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#salesforce">Salesforce</a></strong></td>
    <td>Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR<br>Solutions Architects<br>Marketing OPS<br>Product Marketing<br>Marketing Programs</td>
    <td>Not related to quotes or lead routing: <strong>Francis Aquino (primary)</strong><br>Michael Snow<br><br>Lead Routing/Scoring/Source: <strong>JJ Cordz</strong></td>
    <td>Francis Aquino<br>JJ Cordz<br>Michael Snow</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#screaming-frog">Screaming Frog</a></strong></td>
    <td>Online Growth</td>
    <td><strong>Shane Rice</strong><br>LJ Banks</td>
    <td>Shane Rice</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#sertifi">Sertifi</a></strong></td>
    <td>Account Executives<br>Account Managers</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
    <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#snowflake">Snowflake (Data Warehouse)</a></strong></td>
    <td>Data and Analytics Team<br>Meltano Team</td>
    <td><strong>Taylor Murphy</strong><br>Thomas La Piana</td>
    <td>Taylor Murphy</td>
  </tr>
  <tr>
    <td><strong>Sprout Social</strong></td>
    <td>Content Marketing<br>Marketing OPS</td>
    <td><strong>JJ Cordz</strong><br>Emily von Hoffmann</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#stripe">Stripe</a></strong></td>
    <td>Finance Team</td><td><strong>Wilson Lau</strong></td><td>Wilson Lau</td>
  </tr>
  <tr>
    <td><strong>Survey Monkey</strong></td>
    <td>Marketing</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#terminus">Terminus</a></strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Marketing Programs</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>Tweetdeck</strong></td>
    <td>Varies</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="#">Visual Compliance (Link to come)</a></strong></td>
    <td>Access via Salesforce</td>
    <td><strong>Jamie Hurewitz (primary)</strong><br>Related to Technical Implementaion: Michael Snow (secondary)</td>
    <td>Michael Snow</td>
  </tr>
  <tr>
    <td><strong>WebEx</strong></td>
    <td>Marketing OPS<br>Public Sector</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#xactly">Xactly</a></strong></td>
    <td>Sales Leadership<br>Finance Team</td><td><strong>Francis Aquino (primary)</strong><br>Wilson Lau</td>
    <td>Francis Aquino<br>Wilson Lau</td>
  </tr>
  <tr>
    <td><strong>YouTube</strong></td>
    <td>Varies</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#zendesk">Zendesk</a></strong></td>
    <td>Access via Salesforce</td>
    <td><strong>Lee Matos</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>Zoom</strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>PeopleOPS</strong></td>
    <td>PeopleOPS</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#zuora">Zuora</a></strong></td>
    <td>Access via Salesforce</td>
    <td><strong>Wilson Lau (primary)</strong><br>Francis Aquino (secondary)</td>
    <td>Francis Aquino</td>
  </tr>
</table>
</div>

Also see "Operations License Tracking & Contract Details" which can be found on the Google Drive.   

#### Requesting Access to Tech Stack applications

Access to the above applications should be requested by opening an issue [here](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request)

#### Asking for Help with Salesforce, Outreach, or other tools in our Tech Stack

Questions about usage, routing, or integration concerns for applications should be submitted as an issue using the following template in [_issues.help](
https://gitlab.com/gitlab-com/business-ops/Issues.help/issues/new?issuable_template=Question_Salesforce_Outreach&issue%5Btitle%5D=Tool_Question:%20Write%20Here&issue%5Bdescription%5D)       

#### Integrating Other tools  

The tech stack above is the approved GitLab tech stack. No one is permitted to integrate any other tool into the tech stack without asking Operations for permission as it affects our system security and database integrity.  


### Export Control Classification, and Countries We Do Not Do Business In

GitLab's Export Control Classification (or ECCN) is 5D992.c. As a consequence of this classification, we currently do not do business in: Iran, Sudan (excluding South Sudan), Syria, North Korea, and Cuba.


## Glossary

| Term | Definition |
| :--- | :--- |
| Accepted Lead | A lead a Sales Development Representative or Business Development Representative agrees to work until qualified in or qualified out |
| Account | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller |
| AM | Account Manager |
| AE | Account Executive |
| APAC | Asia-Pacific |
| BDR | Business Development Representative |
| CS | Customer Success |
| EMEA | Europe, Middle East and Asia |
| EULA | End User Licence Agreement |
| High intent | an event, webcast, demo that is a strong indicator of purchase or evaluation |
| Holdover Account | Account ownership retained by someone other than the territory owner |
| Inquiry | an Inbound request or response to an outbound marketing effort |
| IQM | Initial Qualifying Meeting |
| LATAM | Latin America (includes all of Central & South America) |
| MQL | Marketo Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behavior lead scoring) |
| NCSA | North, Central, South America (legacy region being phased out) |
| NORAM | North America |
| Qualified Lead | A lead a Sales Development Representative or Business Development Representative has qualified, converted to an opportunity and assigned to a Sales Representative (Stage `0-Pending Acceptance`) |
| RD | Regional Director |
| ROW | Rest of World |
| SAL | Strategic Account Leader |
| Sales Admin | Sales Administrator |
| Sales Serve | A sales method where a quota bearing member of the sales team closes the deal |
| [SAO] | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Initial Qualifying Meeting |
| SDR | Sales Development Representative |
| Self Serve | A sales method where a customer purchases online through our web store |
| SLA | Service Level Agreement |
| SCLAU | Abbreviation for SAO (Sales Accepted Opportunity) Count Large and Up |
| SQO | Sales Qualified Opportunity |
| TAM | Technical Account Manager |
| TEDD | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company |
| Won Opportunity | Contract signed to Purchase GitLab |

[SAO]: /handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao

## Customer Lifecycle

A customer lifecycle is a term used to track the different milestones prospective customers go through as they learn about GitLab and interact with our sales and marketing teams. Each subsequent milestone is a subset of the previous milestone, and represents a progression from not knowing GitLab to being a customer (and fan) of GitLab.

At the highest level of abstraction, we use the terms `lead` `opportunity` and `customer` to represent a person's progression towards becoming a customer of GitLab. Those three terms also correspond to record types in salesforce.com

Lead => Opportunity => Customer

However, there are more granular steps within the above milestones that are used to track the above process with more precision. They are tracked as follows:

| Funnel stage | Record Type | Status or Stage |
|---------------|--|----------------------|-------------------|
| Raw | [Lead or Contact] | Raw |
| Inquiry | [Lead or Contact] | Inquiry |
| Marketo Qualified Lead | [Lead or Contact] |  MQL |
| Accepted Lead | [Lead or Contact] |  Accepted |
| Qualifying | [Lead or Contact] |  Qualifying |
| Pending Acceptance | [Opportunity] | 0 - Pending Acceptance |
| [Sales Accepted Opportunity] | [Opportunity] | 1 - Discovery |
| Sales Qualified Opportunity | [Opportunity] | 2 Scoping - 5 Negotiating |
| Customer | [Opportunity] | 6-Closed Won |

For the definition of the stage please click the record type link.

When going from Qualifying to Qualified Lead the lead is duplicated to an opportunity, and the lead is set to qualified and not being used anymore.

[Lead or Contact]: #lead--contact-statuses
[Opportunity]: #opportunity-stages
[Sales Accepted Opportunity]: #criteria-for-sales-accepted-opportunity-sao

## SAL Sales Capacity
The following calcuation is used to measure/plan for sales capacity.  This is a calculation only and should be used to set goals and plan.  Sales capacity is an individual metric that is based on several factors (lead source, tenure of salesperson, competitive landscape, geographic location, rep productivity) not included in the formula below:

* Days to close - 180 days
* Months to close - 6 months
* Win rate - 33%
* Months to lose - 3 months (half of months to close, expecting constant fallout)
* Months on average - 4 (33.3% times 6 and 66.7% of 3)
* Capacity Goal (active opportunities in [Stage 1-6](#customer-lifecycle)) - 40 opportunities
* [SCLAU](#glossary) per month - 10 (40 opportunities / 4 months)


## How tos & Setup  

### Webcasts

Webcasts are an excellent tool used by Sales, Marketing and Customer Success to communicate with prospects, customers and partners.

#### Who to work with

* Landing Page Creation - Marketing Web Developer/Designer
* Design assets for webcast landing page & promotion - Designer
* Content creation/verification - Content Marketing
* Zoom set up, SFDC & Marketo program creation - Marketing Programs Manager
* Registration confirmation email - Marketing Programs Manager
* Mass invitation, reminder and follow up email blasts to our marketing database - Marketing Programs Manager

#### Best Practices

1. Give yourself **at least** 30 days of promotion.
2. Send invitation emails 3 weeks out, 2 weeks out, 1 week out, and 2 hours before event. [Sample emails can be found here](https://docs.google.com/document/d/1_Vw-KgOJp-qHQJWP5uir99tAfPqy3VL9hFEmvnQLfeA/edit#).
3. Only send promotional emails Tuesday, Wednesday, or Thursday for optimal results.
4. Send reminder emails to registrants 1 week out, day before, and one hour before the event.
5. Host webcasts on a Wednesday or Thursday, see [note below](#zoom) about scheduling.
6. Post links to additional, related resources during the event.
7. Include "contact us" information at the end of the presentation.
8. Video recording of webcast uploaded to YouTube same day as event occurred.
9. Send the recording to all registrants, whether they attended or not within 24 hours post webcast.
10. Publish a post-webcast blog post capturing some key insights to encourage on-demand registrations.
11. Add webcast to the GitLab resources page.

#### Speaker Approval

We sometimes depend on GitLab's subject matter experts to deliver webcast presentations. However, we must ensure that when we ask a speaker to participate on a webcast that the work is approved. Please use the following guideline when asking a subject matter expert
to participate on a webcast.

1. Have an abstract of the content prepared before asking for a presenter.
2. Send the abstract to both the proposed speaker and their manager to review. **A speaker is not considered booked unless they have approval from their manager.**
3. Address and resolve any concerns regarding the abstract.
4. Once the manager approves and the speaker accepts, you can move forward with the webcast.

#### Tips for Speakers

Here are some basic tips to help ensure that you have a good experience preparing for and presenting on a webcast.

##### Before Committing  
{:.no_toc}
Ask us any questions you have about the time commitment etc. and what exactly our expectations are. Talk about it with your manager if you're on the fence about your availability, bandwidth, or interest. Make sure you're both on the same page. We want this to be a meaningful professional development exercise for you, not a favor to us that you're lukewarm about — if you feel that way, none of will be able to do our best job. We'll be honest with you, so please do the same for us.

##### Before the Dry Run  
{:.no_toc}   
Select and set up your presentation space. Pick a spot with good wifi, and we recommend setting up an external mic for better audio quality, although this is optional. If you will be presenting from your home, alert your spouse/roommates of the time/date & ask them to be out of the house if necessary. Depending on your preferences and comfort level with public speaking, run through the script several times.

##### Before the Presentation
{:.no_toc}
Try to get a good sleep the night before, and, if the presentation is in the morning, wake up early enough to run through your notes at least once. Review our [Positioning FAQ](/handbook/positioning-faq/), or keep the page handy in case you are asked in the Q&A about how GitLab compares to our competitors.

#### Logistical Setup

##### Marketo & SFDC
{:.no_toc}

**Setting up the event in Marketo & SFDC**

1. Create the webcast program in Marketo
   - Title the webcast in the following format: `YYYYMMDD_{Webcast Title}`. For example, 20170418_MovingToGit
   - Click `salesforce campaign sync` and select `create new` to create campaign in SFDC
1. Update `My Tokens` at the webcast program level
   - DO NOT UPDATE THE `apiKey` or `apiSecret`. These should be the same for every webcast
   - Update the `{{my.zoomWebinarId}}` token with the webinar ID from the Zoom webcast created in step 1
1. Turn on smart campaigns in Marketo
   - Activate the `Zoom Attended` campaign
   - Activate the `Webcast_Registered` campaign
   - Activate the `Registered From Zoom` campaign

**Setting up event follow-up**

1. Update email `attended` and email `no show` with content provided by webcast project manager
1. Approve and send samples to content [at] gitlab [dot] com
1. Schedule email send within `follow-up no show` and `follow-up attended` smart campaigns

##### Zoom
{:.no_toc}

The Marketing OPS Zoom license can only be used for a single session at a time. This license is used for both external webcasts and internal cross-team training. Therefore, when a webcast is requested please confirm there is not going to be a conflict between the pre-scheduled sessions using that license. Schedule no less than 30min between sessions (before & after) so there is less chance of a conflict and allows for a buffer.     

Recurring sessions using the license:   
- Sales Enablement training - Thursdays at 9a Pacific time, weekly

**Setting up the webcast**

1. Create webcast in Zoom
   - Log in to Zoom, and select the webcast from the `Webinars` tab
   - Add webcast project manager as altnerative host and add webcast speakers as panelists
   - Add polling questions to the webcast (if applicable)

**Adding alt-host and panelist to a webcast**
<iframe width="560" height="315" src="https://www.youtube.com/embed/4YvV8AoyqXc?rel=0&amp;controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

**Adding poll questions to a webcast**
<iframe width="560" height="315" src="https://www.youtube.com/embed/QIrRcUIYEwo?rel=0&amp;controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

2. Edit the confirmation and reminder emails under the `email setting` tab.
   - Make sure the registration confirmation email and the reminder emails are set to send from Zoom. As Zoom implements their more robust Marketo integration, we will send from Marketo, but for now we need to send from Zoom to ensure that the correct unique link is sent to registrants.
   - There is limited editing capabilities within Zoom. In the confirmation email you can add a snippet of text after the templatized body text and the footer of the email can be edited. In the reminder email, only the footer can be edited.
3. Set up Marketo integration within Zoom
   - Scroll down and click on the `Integration` tab
   - Click `Edit` next to `Generate Leads in Marketo`
   - Turn on `Send registration information to a Smart Campaign` and select the `Registered From Zoom` smart campaign for the proper webcast program
   - Turn on `Send attendee information to a Smart Campaign` and select the `Zoom Attended` smart campaign for the proper webcast program
   - Select the `ZoomWebinarOtherInfo` custom object, check the following boxes, and select the corresponding Marketo Custom Object Fields:
      - Webinar ID
      - Webinar Topic
      - Q&A
      - Poll
4. Edit the landing page to have the appropriate webcast description, date, time. Click on `branding` and update the header (optional)

**Hosting the webinar**

Will add instructions on how to start the webcast in zoom, promote panelists, polling, etc.

#### Routing
{:.no_toc}

For webcast lead routing rules,  see [global routing rules below](https://about.gitlab.com/handbook/business-ops/#lead-routing).


## Requesting an Email

To request an email, create an issue in [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues), using the `Email-Request-mpm.md` issue template. Be as complete as possible providing all requested detail related to the email, goal, target audience and any other pertinant details.  

Email requests should be submitted **no less than** 48 hours before intended send date so the new request can be added into the responsible Markting Program Manager's (MPM) workflow. The responsible MPM will be determined by type of email requested, [see division of duties](/handbook/marketing/marketing-sales-development/marketing-programs/#responsibilities).   

All email sends, stand-alone or part of an event, require their own SFDC campaign that is synced to the Marketo program. This is the way we track engagement of the email in a clear and transparent way. Then combine that information with the event (if there is one).    

By default **all** email tests will be sent to `content@`, if you would like additional people to receive a test email *prior* to sending it out, please specify that in the request template:
- **Sender Name**: Typically we use GitLab Team for most outgoing communications; for Security Alerts we use GitLab Security. Choosing a name that is consistent with the type and/or content of email being sent is important, if unsure make a note and we will make recommendation  
- **Sender Email Address**: What email address should be used?
- **Subject Line**: 50 character max is preferred
- **Email Body Copy**: Can be a text snippet within issue, clearly identified comment on issue or attach a Google doc with copy
- **Target Date to Send Email**: at a minimum a few days notice is preferred because we need to balancing the number of emails being sent to our database so they are not perceived (or marked) as spam; however, a simple email can be turned in a few hours if absolutely necessary
- **Recipient List**: Emails can be sent to one of the [existing segments](/handbook/marketing/marketing-sales-development/marketing-operations//#email-segments) or a recipient list can be provided as a .csv file  

### Internal Email List  
There is an internal email list, called `INTERNAL - Email Send List` in Marketo that will be included on every outbound email that is sent. If you are a team member and wish to be included on every email that is sent, please post a message to the `#marketing-programs` slack channel and the MPMs will add you to this list.   

If you are an internal team member and wish to subscribe to a segment or segments please subscribe through the [preference center](/company/preference-center/) and you will only be included in those dedicated email sends. 


## Record Management

### MQL Definition
A Marketo Qualified Lead is a lead that has reached a certain threshold, we have determined to be 90 points accumulated, based on demographic, firmographic and/or behavioral information. The "MQL score" defined below is comprised of various actions and/or profile data that are weighted with positive or negative point values.
Any record with a `Person Score` greater than or equal to 50 points will be inserted into the routing flow. Using LeanData every time a `Person Score` is updated, LeanData will run a check to see if the record needs to be processed through the flow.

### MQL Scoring Model
The overall model is based on a 100 point system. Positive and negative points are assigned to a record based on their demographic and firmographic information, and their behavior and/or engagement with GitLab marketing.

The MQL scoring model below is correct as of 30 September 2018. Additional changes are being made and the following will be updated over time.

#### MQL = 90 pts
{:.no_toc}

##### Positive Point Values
{:.no_toc}
  * 100pts
     * Completes self-hosted or SaaS trial request form*
     * Completes a Contact form   
     * Completes a Professional Services Request form 
     * Completes a Public Sector Request form
  * 90 pts
     * Attends [high intent](#glossary) live webcast, livestream or demo
  * 50 pts
     * Title contains “VP, Manager, Director, Senior, or Head”
     * Visits Enterprise trial page but doesn’t complete form
  * 45 pts
     * Watches high intent on-demand webcast, livestream or demo
  * 40 pts
     * `Follow Up Requested` in Conference/Field Event/Speaking Session campaign - manual process when lists are uploaded
     * Attends Live in-person/Owned Event - manual process when lists are uploaded
  * 30 pts
     * `Visited Booth` in Conference/Field Event campaign - manual process when lists are uploaded
     * `Attended` in Speaking Session Event campaign - manual process when lists are uploaded
     * Downloads any gated asset
  * 15 pts
     * Registers for any livestream, webcast or demo
     * Attends low intent live webcast
     * Watches low intent on-demand webcast
  * 10 pts
     * Opts into Newsletter
     * Opts into Security Newsletter
     * Opts into webcast emails
     * Opts into live event/conference emails
     * Visits about.gitlab.com/pricing page
     * Visits about.gitlab.com/features page
  * 5 pts
     * Valid company email
  * 2 pts
     * Visits about.gitlab.com/installation page   

##### Negative Point Values
{:.no_toc}
  * -5 pts
     * Email is @ “gmail, hotmail, yahoo, aol” (or other free-domained email)
  * -10 pts
     * Title is “blank, numerical, developer, engineer”
  * -25 pts
     * Email unsubscribe
     * Multiple career page visits in 7 days  

*In-product trial requests for SaaS and self-hosted are applied using batch method that runs daily at 6a Pacific time.    


### Segmentation
Sales Segmentation is based on `Total Employee` count of the `Global Account`.
  * `Large` = 2,000+ total employees
  * `Mid-Market` = 100-1,999 total employees    
  * `SMB` (Small Business) = 0-99 total employees

### Region/Vertical
  * **Asia Pacific/Rest of World**: Michael Alessio, Regional Director        
  * **Europe, Middle East and Africa**: Richard Pidgeon, Regional Director         
  * **North America - US East**: Mark Rogge, Regional Director      
  * **North America - US West**: Haydn Mackay, Regional Director       
  * **Public Sector**: Paul Almeida, Director of Federal Sales          

### Territories   
#### Maps

##### World
{:.no_toc}
![](/images/handbook/business-ops/2018Q4_World.png)

| Territory    | Region | Large        | Mid-Market | Small Business |
|--------------|--------|--------------|------------|----------------|
| Brazil       | LATAM  | Jim Torres   | Jim Torres |                |
| SoCenAmer    | LATAM  | Jim Torres   | Jim Torres |                |


##### APAC
{:.no_toc}
![](/images/handbook/business-ops/2018Q4_APAC.png)

| Territory    | Region | Large        | Mid-Market   | Small Business |
|--------------|--------|--------------|--------------|----------------|
| ANZ          | APAC   | Sarah Orell  | Sarah Orell  |                |
| Central Asia | APAC   | Julie Manalo | Julie Manalo |                |
| China        | APAC   | Jim Torres   | Jim Torres   |                |
| Japan        | APAC   | Jim Torres   | Jim Torres   |                |
| Korea        | APAC   | Jim Torres   | Jim Torres   |                |
| SE Asia      | APAC   | Julie Manalo | Julie Manalo |                |
| South Asia   | APAC   | Julie Manalo | Julie Manalo |                |


##### EMEA 
{:.no_toc}
![](/images/handbook/business-ops/2018Q4_EMEA.png)

| Territory       | Region - SubRegion | Large (**SAL** - SDR)                                              | Mid-Market     | Small Business      |
|-----------------|--------------------|--------------------------------------------------------------------|----------------|---------------------|
| BeNeLux         | EMEA               | **Toby Thorslund**<br>Brandy Brachthuizer                          | Toby Thorslund | Vilius Kavaliauskas |
| DACH            | EMEA               | **Timo Schuit**<br>Peter Kunkli                                    | Conor Brady    | Vilius Kavaliauskas |
| Eastern Europe  | EMEA               | **TBH - *Conor Brady (temp)***<br>TBH - *Andrew Volianiuk (temp)*  | Conor Brady    | Vilius Kavaliauskas |
| MENA            | EMEA               | **TBH - *Justin Haley (temp)***<br>TBH - *Daisy Miclat (temp)*     | Toby Thorslund | Vilius Kavaliauskas |
| Nordics         | EMEA               | **Toby Thorslund**<br>Brandy Brachthuizer                          | Toby Thorslund | Vilius Kavaliauskas |
| Southern Europe | EMEA               | **Hugh Christey**<br>Anthony Seguillon                             | Conor Brady    | Vilius Kavaliauskas |
| UK&I            | EMEA               | **Justin Haley**<br>Daisy Miclat                                   | Toby Thorslund | Vilius Kavaliauskas |


##### NORAM
{:.no_toc}
![](/images/handbook/business-ops/2018Q4_NORAM.png)
![](/images/handbook/business-ops/2018Q4_NORAMCalifornia.png)

| Territory                 | Region - SubRegion | Large (**SAL** - SDR)                 | Mid-Market                         | Small Business |
|---------------------------|--------------------|---------------------------------------|------------------------------------|----------------|
| Central-Gulf-Carolinas    | NORAM - US East    | **Sean Billow**<br>Kevin McKinley     | Peter McCracken                    | Kyla Gradin    |
| Sunshine Peach            | NORAM - US East    | **David Wells**<br>Jeremy Dezotel     | Peter McCracken                    | Kyla Gradin    |
| Manhattan                 | NORAM - US East    | **John May**<br>Joseph Davidson       | Peter McCracken                    | Kyla Gradin    |
| Mid-Atlantic              | NORAM - US East    | **Larry Biegel**<br>Jenny Beauregard  | Peter McCracken                    | Kyla Gradin    |
| Mid-Central               | NORAM - US East    | **Mark Bell**<br>Matthew Walsh        | Peter McCracken                    | Kyla Gradin    |
| New England - East Canada | NORAM - US East    | **Tony Scafidi**<br>Kaleb Hill        | Peter McCracken                    | Kyla Gradin    |
| North Central - Ontario   | NORAM - US East    | **Jordan Goodwin**<br>Marcus Stangl   | Peter McCracken                    | Kyla Gradin    |
| NY State - New Jersey     | NORAM - US East    | **Paul Duffy**<br>Ryan Kimball        | Peter McCracken                    | Kyla Gradin    |
| Lone Star South           | NORAM - US East    | **Adam Johnson**<br>Adam Pestreich    | Peter McCracken                    | Kyla Gradin    |
| Big Sky                   | NORAM - US West    | **Adam Olson**<br>Michael Miranda     | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |
| Nor Cal                   | NORAM - US West    | **Joe Miklos**<br>Jordan Bushong      | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |
| PNW                       | NORAM - US West    | **Joe Drumtra**<br>Matthew Beadle     | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |
| South-Inland Cal          | NORAM - US West    | **Chris Cornacchia**<br>Shawn Winters | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |
| San Mateo                 | NORAM - US West    | **Joe Miklos**<br>Jordan Bushong      | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |
| Santa Clara               | NORAM - US West    | **Joe Miklos**<br>Jordan Bushong      | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |
| SF-North Bay-HI           | NORAM - US West    | **Mike Walsh**<br>Paul Schofield      | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |
| High Desert               | NORAM - US West    | **Nico Ochoa**<br>Brooke Williamson   | Philip Camillo<br>Matthew Doerfler | Kyla Gradin    |


In Marketo we have modified the country values to match Salesforce to allow for seamless syncronization between the two systems. When creating new forms in Marketo, you can use the "Master Country List" (viewable on the Google Drive), copying column A into the form builder.

### Global Account Ownership
When assigning records, global account ownership will be taken into consideration. By default, any record will be assigned to the account owner of `Ultimate Parent Account` record in Salesforce with exception of [Named Accounts](#named-account-ownership). If it is determined during the sales cyle that the decision related to this inquiry will be made at the subsidiary level, then a new account will be created with complete subsidiary account and address information, and future records that match address information will be assigned by this new account.

For example, `Acme Inc.` has a global headquarters in London. As a result, all inbound records with a domain of `@acme.com` will be assigned to the EMEA team - more specifically, the account owner of the `Acme, Inc` account record in Salesforce. Let' say then, an inbound record with a domain of `@acme.com` is created with an address in Japan. During the sales process, it is determined that `Acme Japan` will ultimately make this decision, and that they will usually make all decisions independent of global HQ. As a result, `Acme Japan` will be created by the OPS team as an account in Salesforce and will be assigned to an APAC team member, and all contacts and opportunities related to this and future opportunities in Japan will be associated to the `Acme Japan` account.

### Named Account Ownership
When assigning records, named account ownership will be taken into consideration. There are two kinds of "Named Accounts":
1. A hold over account that falls outside the SALs defined territory but is an account they will continue to work until a transition to Global/Territory Account Owner has been determined. This transition can either be at the close of an opportunity or the close of an upcoming renewal.         
2. An account that fall outside the SALs defined territory but is an account they will retain ownership of into perpetuity.        

A `Named Account` checkbox will appear on the ACCOUNT object layout, but can only be updated by Sales or Marketing Operations. If you need assistance marking an opportunity as a Named Account, please send a **chatter in Salesforce** to Operations.

#### Example of a Named 'hold over' Account
{:.no_toc}
An account with a headquarters in New York, NY may have been owned by a rep who will cover the Mid-West region. Since the sales engagement began before the territories were created, the Mid-West rep should continue to work the opportunity. However, once the opportunity is either won or lost, an immediate transition to the new rep (i.e. Global Owner) should take place. This should include the following steps:

1. Reassign the account and contact records to the new owner (please make sure not to reassign any won/lost opportunities);
2. Scheduled call to formally handoff the account. The former rep should provide the new rep will as much detail on the situation as they can;
3. Send an email to the client/prospect introducing the new representative.


### Territory Account Ownership  
Accounts are assigned based on territorial ownership. These are not `Named Accounts` these are accounts that are owned by a specific Strategic Account Leader because the HQ and/or buying group is based in the reps designated territory. In many cases, the Global owner and the Territorial owner will be the same person.


### Initial Source
`Initial Source` is first "known" touch attribution or when a website visitor becomes a known name in our database, once set it should never be changed or overwritten. For this reason Salesforce is set up so that you are unable to update both the `Initial Source` and `Lead Source` fields. If merging records, keep the `Initial Source` that is oldest (or set first). When creating Lead/Contact records and you are unsure what `Initial Source` should be used, ask in the #Lead-Questions Slack channel.

The values listed below are the only values currently supported. If you attempt to upload or import leads or contacts into Salesforce without one of these initial sources you will encounter a validation rule error. If you think that there needs to be a new Initial Source added to this list and into Salesforce please slack the appropriate team member(s) listed in the [Tech Stack](/#tech-stack).

The `Initial Source` table below is current as of 10 October 2018.  

Status in the table below means:  
- Active = can be selected from picklist  
- Inactive = cannot be selected from picklist, but a record may exist with this source  

| Source                          | Definition and/or transition plan                                                                            | Status*  |
|:--------------------------------|:-------------------------------------------------------------------------------------------------------------|:---------|
| Advertisment                    | to be evaluated                                                                                              | Active   |
| AE Generated                    | Sourced by an Account Executive through networking or professional groups                                    | Active   |
| CE Download                     | Downloaded CE version of GitLab                                                                              | Active   |
| CE Usage Ping                   | Created from CE Usage Ping data                                                                              | Active   |
| CE Version Check                | to be evaluated                                                                                              | Inactive |
| Clearbit                        | transition to `Prospecting` -> sub field `Clearbit`                                                          | Active   |
| Conference                      | Stopped by our booth or received through event sponsorship                                                   | Active   |
| CORE Check-Up                   | will be activated for new records created by the Instance Review in-product                                  | Inactive |
| Datanyze                        | transition to `Prospecting` -> sub field `Datanyze`                                                          | Active   |
| Demo                            | Filled out form to watch demo of GitLab                                                                      | Active   |
| DiscoverOrg                     | transition to `Prospecting` -> sub field `DiscoverOrg`                                                       | Active   |
| Education                       | Filled out form applying to the Educational license program                                                  | Active   |
| EE Version Check                | to be evaluated                                                                                              | Inactive |
| Email Request                   | Used when an email was received through an alias (*will be deprecated*)                                      | Active   |
| Email Subscription              | Subscribed to our opt-in list either in preference center or various email capture field on GitLab website   | Active   |
| Employee Referral               | to be evaluated                                                                                              | Active   |
| Event partner                   | to be evaluated                                                                                              | Inactive |
| Field Event                     | Paid events we do not own but are active participant (Meetups, Breakfasts, Roadshows)                        | Active   |
| Gated Content - General         | Download an asset that does not fit into the other Gated Content categories                                  | Active   |
| Gated Content - Report          | Download a gated report                                                                                      | Active   |
| Gated Content - Video           | Watch a gated video asset                                                                                    | Active   |
| Gated Content - White Paper     | Download a white paper                                                                                       | Active   |
| Gemnasium                       | Previous a Gemnasium customer/prospect merged into our database when acquired                                | Active   |
| GitLab Hosted                   | GitLab Hosted customer/user                                                                                  | Active   |
| GitLab Subscription Portal      | Account created through the Subscription app (check for duplicates & merge record if found)                  | Inactive |
| GitLab.com                      | Registered for GitLab.com account                                                                            | Active   |
| Gitorious                       | Previous a Gitorios customer/prospect merged into our database                                               | Active   |
| gmail                           | unknown, to be deprecated                                                                                    | Inactive |
| InsideView                      | transition to `Prospecting` -> sub field `InsideView`                                                        | Inactive |
| Leadware                        | transition to `Prospecting` -> sub field `Leadware`                                                          | Active   |
| Legacy                          | to be evaluated                                                                                              | Active   |
| LinkedIn                        | transition to `Prospecting` -> sub field `LinkedIn`                                                          | Active   |
| Live Event                      | transition to correct category based on first event attended -> `Owned Event`; `Field Event` or `Conference` | Active   |
| MovingtoGitLab                  | to be evaluated                                                                                              | Inactive |
| Newsletter                      | to be evaluated                                                                                              | Active   |
| OnlineAd                        | to be evaluated                                                                                              | Inactive |
| OSS                             | Open Source Project records related to the OSS offer for free licensing                                      | Active   |
| Other                           | Should never be used but is a legacy source that will be deprecated                                          | Active   |
| Owned Event                     | Events that are created, owned, run by GitLab                                                                | Active   |
| Partner                         | GitLab partner sourced name either through their own prospecting and/or events                               | Active   |
| Promotion                       | to be evaluated                                                                                              | Active   |
| Prospecting                     | Account research and developement prospecting work                                                           | Pending  |
| Prospecting - LeadIQ            | transition to `Prospecting` -> sub field `LeadIQ`                                                            | Active   |
| Public Relations                | to be evaluated                                                                                              | Active   |
| Referral                        | to be evaluated                                                                                              | Inactive |
| Registered                      | transition to correct event type source                                                                      | Inactive |
| Request - Contact               | Filled out contact request form on GitLab website                                                            | Active   |
| Request - Professional Services | Any type of request that comes in requesting to engage with our Customer Success team                        | Active   |
| Sales                           | to be evaluated                                                                                              | Inactive |
| SDR Generated                   | Sourced by an SDR through networking or professional groups                                                  | Active   |
| Security Newsletter             | Signed up for security alerts                                                                                | Active   |
| Seminar - Partner               | not actively used - transition to `Owned Event` or `Field Event`                                             | Active   |
| SocialMedia                     | to be evaluated                                                                                              | Inactive |
| Swag Store                      | to be evaluated                                                                                              | Inactive |
| Trial - Enterprise              | In-product or web request for self-hosted Enterprise license                                                 | Active   |
| Trial - GitLab.com              | In-product SaaS trial request                                                                                | Active   |
| Unknown                         | need to evaluate what records are in this status - it should never be used                                   | Inactive |
| Unsubscribe Form                | to be evaluated                                                                                              | Inactive |
| Web                             | transition to `Web Direct`                                                                                   | Active   |
| Web Chat                        | Engaged with us through website chat bot                                                                     | Active   |
| Web Direct                      | Created when purchase is made direct through the portal (check for duplicates & merge record if found)       | Active   |
| Webcast                         | Register for any online webcast (not incl `Demo`)                                                            | Active   |
| Word of Mouth                   | to be evaluated                                                                                              | Active   |
| Zoominfo                        | transition to `Prospecting` -> sub field `Zoominfo`                                                          | Inactive |


### Lead & Contact Statuses
The Lead & Contact objects in Salesforce have unified statuses with the following definitions. If you have questions about current status, please ask in #lead-questions channel on Slack.

| Status | Definition |
| :--- | :--- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketo Qualified through systematic means |
| Accepted | Actively working to get in touch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |

### Routing
Routing is determined by `Sales Segmentation`, `Region`, and `Global Account Ownership`. Routing through Lean Data when a record has no less than 30 points, this means that they have engaged with at least one piece of content or visited a high value page.     

#### NORAM  
* Named Accounts = Any inbound request (Trial - Enterprise/SaaS, Contact Us, Webcast, Content download) that matches a Named Account will be routed to the Account Owner & SDR     
* Territorial/Geo patch = Webcast and/or Content downloads that belong to a territory/geographical patch will be routed to the Account Owner & SDR      
* `Sales Segment` = `SMB` = Any inbound request (Trial - Enterprise/SaaS, Contact Us, Webcast, Content download) will be routed to the SMB Customer Advocate   
* Public Sector = Any inbound request (Trial - Enterprise/SaaS, Contact Us, Webcast, Content download) will be routed to the Public Sector team based on type of entity (Government, Local, etc)     
* Unnamed Account and/or `Sales Segment` - `Unknown` = any inbound request (Trial - Enterprise/SaaS, Contact Us) will be routed to the BDR team to qualify   

#### EMEA  
* `Sales Segment` = `Large` - Any inbound request (Trial - Enterprise/SaaS, Contact Us) will be routed to the Owner of the `Region` & associated SDR   
* Territorial/Geo patch = Webcast and/or Content downloads that belong to a territory/geographical patch will be routed to the territory owner & SDR   
* `Sales Segment` = `SMB` = Any inbound request (Trial - Enterprise/SaaS, Contact Us) will be routed to the Customer Advocate team   
* All other inbound requests (`Sales Segment` = `Mid-Market` or `Unknown`) will be routed to the EMEA BDR   

#### APAC     
* `Sales Segment` = `Large` - Any inbound request (Trial - Enterprise/SaaS, Contact Us) will be routed to the Owner of the `Region` & XDR team
* Any inbound request will be routed to the APAC XDR (hybrid BDR/SDR) team     
* `Sales Segment` = `SMB` = Any inbound request (Trial - Enterprise/SaaS, Contact Us) will be routed to the NORAM Customer Advocate team


#### Contact Requests
All `Contact Us` requests must be followed up within **one (1) business day** Service Level Agreement (SLA) and follow up must be tracked as an activity on the record within Salesforce.

Contact requests are routed in the following manner:   
- NORAM
    - `Sales Segment` = `Large` -> Territory Owner & SDR     
    - `Sales Segment` = `SMB` -> Customer Advocate team     
    - `Industry` = `Public Sector` -> Public Sector team    
    - `Sales Segment` = `Mid-Market` or `Unknown` -> BDR team    

- EMEA
    - `Sales Segment` = `Large` -> Territory Owner & SDR    
    - `Sales Segment` = `SMB` -> Customer Advocate team     
    - `Sales Segment` = `Mid-Market` or `Unknown` -> BDR team     

- APAC
    - `Sales Segment` = `Large` -> Territory Owner      
    - `Sales Segment` = `SMB` -> Customer Advocate team     
    - `Sales Segment` = `Mid-Market` or `Unknown` -> XDR team   


U.S. Public Sector: Routed to the U.S. Public Sector SDR and Inside Sales team.
- [GitLab Public Sector Rules of Engagement](/handbook/sales/public-sector/engaging-public-sector.html)

#### Professional Service Requests    
`Professional Service Requests` are treated like a [`Contact Us` request](#contact-us-requests) and followed up within **one (1) business day** Service Level Agreement (SLA). Follow up must be tracked as an activity on the record within SFDC.   

Requests submitted through the professional services page will be routed following `Global Account Ownership` rules. Notification of form submission will be sent to the Strategic Account Leader (SAL) and Sales Development Representative (SDR) as well as Customer Success manager. Initial response to form submission is the responsibility of the Account Owner (i.e. SAL).     

#### Trial Requests
Trials can be requested through [web form](/free-trial/) or within product UI for both self-hosted Enterprise or SaaS. Default trial length is thirty (30) days.     
Trial requests are routed in the following manner:  
- NORAM
     - Named Account routed to the Account Owner & SDR
     - `Sales Segment` = `SMB` will be routed to Customer Advocate team   
     - United States Public Sector will be routed to the Public Sector team
     - All others will be routed to the BDR team
- EMEA
     - `Sales Segment` = `Large` will be routed to the Territory Owner & SDR
     - `Sales Segment` = `SMB` will be routed to the Customer Advocate team
     - All others will be routed to the EMEA BDR team   
- APAC
     - `Sales Segment` = `Large` will be routed to the Territory Owner & SDR
     - All requests will be routed based on geography to XDR (hybrid BDR/SDR) team



### Record Ownership
Contacts on accounts owned by a member of the Field Sales Team (RD/SAL/AE/AM), will be owned by the named Account Owner (i.e both the Account and Contact ownership will match). When an SDR is assigned to an Account to support and assist with outreach, the SDR will be added to the `GitLab Team` on the Account object within SFDC, which then populates down to the related Contact records.

Records, both `LEAD` and `CONTACT`, need to be synced to Outreach to ensure email activity is properly mapped back to `Activity History` in SFDC. The owner of the record in SFDC **does not** need to match the owner in Outreach.  


#### Record Creation in Salesforce   
ACCOUNT/CONTACT records in Salesforce are created in a number of ways - [list imports](#list-imports), [mass creation screen flows](#mass-create-contact-on-opportunities-with-contact-roles), field event booth scans, research, networking, webcasts, content downloads. Ideally all ACCOUNTS exist in Salesforce and team members are only creating CONTACT records; however, if a connection is made at an event and follow up needs to be done *prior* to official event list upload occurs team members should do the following:   
   - Search Salesforce to be sure ACCOUNT does not already exist **AND** search using the person's email address to ensure duplicate record is not created
   - Record **does not** exist:
        - Create `Standard` ACCOUNT type - required fields are `Account Name` & `Account Type`
        - Create `Standard` CONTACT type - required fields are `Last Name`, `Account Name` (use lookup tool to find ACCOUNT just created) & `Initial Source` (i.e. where is this name coming from `Conference` = Field Event, `SDR Generated` or `AE Generated` = regular networking event, etc)   
        - Be accurate where the name is collect from, `Unknown` is **not** acceptable.
        - The `Initial Source` on a CONTACT record *does not* equal `Source` on an opportunity. Refer to [`Initial Source`](#initial-source) for guidance on why this is important.    
   - Record **does** exist:
        - If LEAD or CONTACT is unowned or "owned by Sales Admin, JJ Cordz, Francis Aquino or Chad Malchow", this record is adoptable by anyone - change `Record Owner` to your name  
        - If LEAD or CONTACT is owned by BDR team member, **before** reaching out Chatter on the record asking BDR to transfer ownership. Ownership *must* be transfered **before** reaching out to avoid confusion, cross-communication and/or multiple people reaching out to same contact.   

When official event list import is completed the created ACCOUNT or CONTACT record will be appended with the additional data collected at the event. If there are any questions or you need assistance, please ping Marketing or Sales OPS in Slack `#sfdc-users` channel.      

##### Best practices   
1. Be as accurate with your data as possible. If you do not have a Contact `Phone` do not substitute the Account `Phone` - Leave it blank on the Contact record.  
2. Search the database for duplicates before creating new records.  
3. When merging records, retain the `Initial Source` of the record that was created first.   


### Mass create contacts on opportunities with contact roles

This process is specific for the unique cases of creating totally new contacts for an account that also have to be associated with an opportunity. An example of when to use this process is when you meet a number of new contacts at an account during a demo for a specific opportunity. The process to create these contacts for that account and to have them related with that opportunity would be through the steps listed below or as demonstrated in this [video](https://drive.google.com/file/d/1iEO4dQUAfa4tkbEmip1Xne7-9Tg2nR6h/view?usp=sharing): 

1. Navigate to the Opportunity record in Salesforce that these contacts should be associated with
2. Click on the button `New Contacts & Opp Contact Roles`
3. Follow the screen flow and instructions for creating all contacts and associating them with the opportunity, providing all required and known information 
4. If there is additional information for any of the contacts that you have but you were not able to input the informaiton via the screen flow, navigate to the contact(s) once the operation is complete and fill in any additional information you have for the contact(s)

It is important to note that by following this process that all contacts must meet the following critieria:
1. All of the contacts that are created are to be associated with both the account and opportunity for that opportunity record.
2. All of the contacts are net new and do not exist within Salesforce already - as either contacts or leads.
3. All of the contacts will be assigned a contact role on the opportunity.
4. There already is a primary contact, or one of the new contacts will be the primary contact on the opportunity. 


#### List Imports

There are three primary ways to import records into Salesforce:   
- Direct download from DiscoverOrg using the visualforce page within Salesforce - [instruction video how to do this](https://drive.google.com/file/d/1saQqwLuVpy50LfIH0tkXY_dUt4Rybxth/view?usp=sharing)   
- Download from DiscoverOrg as .csv that needs to be manually uploaded to Salesforce
- List from a field event, sponsorship or advertising  

List imports are done by the Operations team and all list import records are input to Salesforce as CONTACTS. **List upload needs to be done *before* any kind of follow up or outreach is done so we can ensure proper order of operations & attribution is given correctly.**   

The following data cleanup is required for any list prior to sending it to the Operations team. If your spreadsheet/data does not meet these guidelines it will returned to you to fix prior being uploaded.  
   - Preferred format is .csv, but will accept an .xls, or .xlsx       
   - All fields are separated into their own column    
            - Person name separated into two columns - `First Name` `Last Name`    
            - Address separated into individual fields (`Street`, `City`, `State/Province`, `Zip/Postal Code`, `Country`)    
            - Specify if address is for the CONTACT or the ACCOUNT    
            - `Country` that **are not** `United States` or `Canada` *must* have `State` field deleted or cleared as it will create conflicts   
   - Record ownership will be assigned using the [Global Ownership](#global-account-ownership) rules    
   - ADD column for `Source` and provide the source where the names came from (LinkedIn, DiscoverOrg, Event, etc) - see [Initial Source definitions](#initial-source) if unsure  
   - If part of a campaign, ADD column for `Campaign ID` and `Campaign Member Status` 
   - If there are any records who have opted out of contact for any reason please be sure to define that on the spreadsheet

Best Practices
1. Account or Company Name needs to be consistent throughout the entire spreadsheet, please review to ensure that all members of a single company are the exact same including punctuation        
     - Example: GitLab, Gitlab, gitlab - will create three different accounts when the list is uploaded to Salesforce   
2. Remove inaccurate entries
     - `Title` **remove** "self", "me", "n/a", etc
     - `Phone` **remove** obvious junk numbers 0000000000, 1234567890, etc
3. **Blank fields** are better than junk data. We have enrichment tools that are designed to write to blank fields. Also we can run reports on the blank fields to find where our data gaps are.
4. If you do not have a CONTACT `Phone` **do not** substitute the ACCOUNT `Phone` and vice versa. Leave it blank.   
4. Sort spreadsheet by `Email Address` and remove duplicates.    
5. Only records from authorized sources that have verifiable GDPR compliance will be flagged as `GDPR Compliant`.  
     - Pulling list of names out of LinkedIn and importing the records into SFDC **does not** qualify as compliant.   
     - Field events that have not gained consent from the attendees that their name will be shared **are not** compliant.
     - Getting someone's name and/or business card from a meetup **does not** qualify as compliant.

File will be reviewed and will be returned to you if the data does not meet the above cleanup requirements. Once the file has accepted, please allow up to a five (5) day turnaround time for DiscoverOrg or other list import. Field event lists will be uploaded and associated to correct campaign within 24 hours of receipt.   


#### Rules of Engagement

Named Accounts are owned and worked by the designated Strategic Account Leader (SAL) and the paired Sales Developmemt Representative (SDR). This pairing owns all records (LEADS and CONTACTS) associated to a Named Account and any related Child accounts within SFDC. See [global ownership](#global-account-ownership) and [named account](#named-account-ownership) descriptions above.    

Territories are assigned based on [Sales Segmentation](#segmentation) and routing for each type of inbound request is [listed above](#lead-routing).  


LEAD/CONTACT Records with the `Initial Source` of `GitLab.com` are **not** to be engaged, prospected or targeted unless they have taken a handraising 'active' activity, such as `Trial - Enterprise`, `Trial - GitLab.com`, `Contact Us`, `Demo`, 'Webcast', 'Content' and/or engaged in `Web Chat`.

For information about GitLab's email policy and the types and number of emails we send, please see our [Email Communication Policy](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-operations/index.html#email-communication-policy).

#### Active vs. Passive

`Initial Source` cannot be used to determine if a lead is 'active' or 'passive' since the Initial Source is set upon first touch attribution; therefore, looking at the `Last Interesting Moment` field is the primary field used to begin determining if a record is actively being worked. Reviewing the `Activity History` in Salesforce is another factor considered when evaluating 'active' or 'passive'.

A LEAD or CONTACT is considered 'Active' if they have taken an `Trial - Enterprise`, `Trial - GitLab.com`, attended a high intent live webcast or demo and/or engaged `Web Chat`, these are all handraising 'active' activities. These types of records are considered 'Active' for a minimum of 60 days from the date of the handraising activity. For example: the record is considered 'Active' for entire duration of EE trial, plus 30 days after `EE Trial End Date`.


## Campaigns
Campaigns are used to track efforts of marketing tactics - field events, webcasts, content downloads. The campaign types align with how marketing tracks spend and align the way records are tracked across three of our core systems (Marketo, Salesforce and Bizible) for consistent tracking. Leveraging campaign aligns our efforts across Marketing, Sales and Finance.     

### Campaign Type & Progression Status    
A record can only progress **one-way** through a set of event statuses. A record *cannot* move backward though the statuses.  

i.e. Record is put into `Registered` cannot be moved backward to `Waitlisted`


#### Conference     
Any large event that we have paid to sponsor, have a booth/presence and are sending representatives from GitLab (example: AWS). This is tracked as an *offline* Bizible channel.     

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Meeting Requested               | Meeting set to occur at conference                                                    |          |
| Meeting No Show                 | Scheduled meeting at conferenve was cancelled or not attended                         |          |
| Meeting Attended                | Scheduled meeting at conference was attended                                          | Yes      |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |

#### Field Event      
This is an event that we have paid to participate in but do not own the registration or event hosting duties (example: Rancher event). This is tracked as an *offline* Bizible channel. 

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        |          |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |

#### Gated Content      
White Paper or other content offer. This is tracked as an *online* Bizible channel.   

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Downloaded                      | Downloaded content                                                                    | Yes      |

#### Inbound Request      
Any type of inbound request that requires follow up. This is tracked as an *online* Bizible channel.  

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Requested Contact               | Filled out Contact, Professional Services, Demo or Pricing Request                    | Yes      |

#### Owned Event      
This is an event that we have created, own registration and arrange speaker/venue (example: Gary Gruver Roadshow). This is tracked as an *online* Bizible channel.  


| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |

#### Speaking Session
This campaign type can be part of a larger Field/Conference/Owned event but we track engagement interactions independently from the larger event to measure impact. It is something we can drive registration. It is for tracking attendance at our speaking engagements. This is tracked as an *offline* Bizible channel.   


| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Registered                      | Registered or indicated attendance at the session                                     |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended speaking session event                                                       | Yes      |
| Follow Up Requested             | Had conversation with speaker or requested additional details to be sent post event   | Yes      |

#### Virtual Sponsorship     
A virtual event that we sponsor and/or participate in that we do not own the registration but will generate a list of attendees, engagement and has on-demand content consumption post-live virtual event. This is tracked as an *offline* Bizible channel. 

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing targeted email                                                              |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        |          |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |
| Attended On-demand              | Watched/consumed the presentation materials post-event on-demand                      | Yes      |

#### Webcast     
Any webcast that is held by GitLab or a sponsored webcast with a partner. This is tracked as an *online* Bizible channel. 

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Registered                      | Registered through online form                                                        |          |
| No Show                         | Registered but did not attend live webcast                                            |          |
| Attended                        | Attended the live webcast                                                             | Yes      |
| Attended On-demand              | Watched the recorded webcast                                                          | Yes      |


## Opportunities

### Criteria for Sales Accepted Opportunity (SAO)

The following criteria are **required** for all SAOs:

* Confirmation of right person/right profile - the prospect you are speaking with is involved in any project or team related to the potential purchase of GitLab, either as an evaluator, decision maker, technical buyer, or influencer and is interested in hearing more about GitLab.
* Confirmation of the business problem(s) the prospect has?  Why they are looking at GitLab?
* You do not need full BANT to deem an opportunity as sales accepted and to move it to stage 1-Discovery. All that stage 1-Discovery means is that you are in the process of prospect discovery: you are qualifying the opportunity.

* During the BDR/SDR qualification, several data points may be captured, but are **not required** for an SAO:
  * The immediate number of seats or users that the prospect may be interested in purchasing.
  * The prospect's preference on how they would like to deploy GitLab (hosted or self-managed).
  * Technology stack- what solutions are in place today.
  * Solutions to be replaced- from the list of tools that are in place, which of those solutions are up for replacement.
  * Competition- a list of solutions that we will compete with.


### How to create an Opportunity   
An OPPORTUNITY can be created in Salesforce a) when converting a LEAD to CONTACT; b) from a CONTACT. **All opportunities** should be created with a Stage = `00-Pre Opportunity` regardless of how you create the OPPORTUNITY. Once the initial setup is complete, the [OPPORTUNITY Stage](#opportunity-stages) can updated based on the criteria below. 

#### Creating a New Business Opportunity from CONTACT record   
{:.no_toc}   
1. On CONTACT record, click the `New Opportunity` button. Required fields are:
     * Opportunity Name - using [Opportunity Naming Convention](#opportunity-naming-convention)
     * Account Name = This should NOT need to be changed as it pulls from the CONTACT object
     * Type = `New Business`
     * Initial Source = DO NOT CHANGE  
     * Close Date = if no time frame defined, **BDR** set close date rolling 4-months; **SDR** set close date rolling 9-months
     * Stage = `00-Pre Opportunity` - starting stage for ALL opportunities
     * Add as much detail on the OPPORTUNITY record as you can. 
     * Click `SAVE`
2. Scroll down OPPORTUNITY record to the `Contact Roles` section, **click** `New`. CONTACTS associated to the ACCOUNT will be listed (up to 50 records). You must select a CONTACT as **Primary** and define the `Role`.
     * If you do not define a **Primary** CONTACT marketing attribution & activity influence on the OPPORTUNITY will not be accurately captured. 
3. Change the OPPORTUNITY Owner to the `Account Owner` (i.e. SAL/AM). Click `Save`. 
4. Within the OPPORTUNITY record, click the `Initial Qualifying Meeting` button. Enter the required fields (Start/End dates, Type) and update the description field with any notes the SAL/AM should have and review *before* taking the scheduled meeting.
     * Fill in the `Related to` section for BOTH the CONTACT and the OPPORTUNITY
     * Change the `Assigned to` field to the OPPORTUNITY owner
     * Click `Save`
5. Update the OPPORTUNITY Stage from `00-Pre Opportunity` to the correct stage -> normally `0-Pending Acceptance`. 

#### Creating a New Business Opportunity from LEAD record  
{:.no_toc}
1. On LEAD record, fill out the required qualification questions, add additional notes to the optional sections if gathered AND update to `Lead Status` = `Qualified`. Click **`Save`**. 
2. Click the `Convert` button: 
     * Change `Record Owner` to the Account Owner (based on [Global Ownership rules](#global-account-ownership))
     * Check the "Send Email to the Owner" box
     * Lookup the correct `Account Name` - if unsure assign OPPORTUNITY to the "Parent" account
     * Opportunity Name - using [Opportunity Naming Convention](#opportunity-naming-convention)
     * Click `CONVERT`
          * If CONTACT record exists, associate converted LEAD to existing CONTACT. *Do not create duplicate if possible*
3. The OPPORTUNITY will need to be updated with the following: 
     * Click `Edit`
     * Type = `New Business`
     * Initial Source = DO NOT CHANGE  
     * Close Date = if no time frame defined, **BDR** set close date rolling 4-months; **SDR** set close date rolling 9-months
     * Add as much detail on the OPPORTUNITY record as you can. 
     * Click `SAVE`
4. Scroll down OPPORTUNITY record to the `Contact Roles` section, the converted LEAD will automatically be set as **"Primary"**. Click `Edit` and define the `Role`.     
     * Add any additional CONTACTS and define their `Role` that need to be associated with the OPPORTUNITY
     * Opportunities must have a **Primary** CONTACT defined so marketing attribution & activity influence is accurately captured.
Change the OPPORTUNITY Owner to the `Account Owner` (i.e. SAL/AM). Click `Save`. 
5. Within the OPPORTUNITY record, click the `Initial Qualifying Meeting` button. Enter the required fields (Start/End dates, Type) and update the description field with any notes the SAL/AM should have and review *before* taking the scheduled meeting.
     * Fill in the `Related to` section for BOTH the CONTACT and the OPPORTUNITY
     * Change the `Assigned to` field to the OPPORTUNITY owner
6. Update the OPPORTUNITY Stage from `00-Pre Opportunity` to the correct stage -> normally `0-Pending Acceptance`.      

#### Creating an Add-on Opportunity
{:.no_toc}
An `Add-On` OPPORTUNITY will inherit information from the *original* `New Business` OPPORTUNITY. The steps to create an `Add-on` OPPORTUNITY varies slightly from the instructions above because this type of OPPORTUNITY is created from the `New Business` OPPORTUNITY **not** from a converted LEAD or CONTACT.   

This creates a parent-child relationship between the *original* `New Business` OPPORTUNITY and the `Add-on` OPPORTUNITY.   

1. Navigate to the *original* `New Business` OPPORTUNITY (this will become the "parent" opp).
     * Example: If you are selling additional seats to an existing subscription - you should go to the original `New Business` OPPORTUNITY. 
2. Click the `New Add-on Opportunity` button.
3. **UPDATE** the OPPORTUNITY Name - see the [Opportunity Naming Convention] guidelines
4. Define:
     * `Initial Source` = see the [definition table](#initial-source) to choose the most correct source. It is important to be accurate as this does impact reporting and marketing attribution. 
     * Close Date = if no timeframe defined input close date on a rolling 9-months. 
     * Stage = All opportunities start as `00-Pre-Opportunity`
5. Add any additional details on the OPPORTUNITY record
6. Click `Save`

Within the parent-child OPPORTUNITY hierarchy some information will pass from the parent (`New Business`) to the child (`Add-on`). This information will be used in our reporting and analysis to track add-on business OPPORTUNITIES to their `Initial Source` and contributing team members. 

There are additional validation rules that are presently in effect: 
- The **Parent** OPPORTUNITY must either be a `New Business` or `Renewal` OPPORTUNITY. 
- A **Parent** OPPORTUNITY *cannot* be another `Add-on` OPPORTUNITY
- All sales-assisted non-portal `Add-on` OPPORTUNITIES **must** have a parent opportunity.

#### Creating a Professional Services Opportunity
{:.no_toc}
A `Professional Services` OPPORTUNITY will be used to cover any integration, consulting, training or other service that a Sales rep will sell to a prospect/client and needs or wants to be invoiced separately. To invoice separately a new quote and opportunity must be created.    
A full list of professional services can be found [here](/handbook/customer-success/implementation-engineering/offerings/).

1. Navigate to the *original* OPPORTUNITY (this will become the "parent" opp).
2. Click the "New PS Opportunity" button and fill out the following:
     * OPPORTUNITY Name = will already be set correctly; do not change
     * Type = do not change it will populate from parent OPPORTUNITY
     * Initial Source = do not change it will populate from parent OPPORTUNITY
     * Close Date = if no timeframe defined input close date on a rolling 9-months. 
     * Stage = All opportunities start as `00-Pre-Opportunity`
     * Professional Services Value = enter dollar value. 
     * ACV = **do not populate** an automated workflow will fill this information
     * Amount = **do not populate** an automated workflow will fill this information
     * Professional Services Description, Project Scope, Task Schedule and Key Assumption fields = these will push to the Statement of Work when a PDF is generated from Zuora. 
     * Verify the `Professional Services` OPPORTUNITY has the *original* OPPORTUNITY in the `Parent Opportunity` field. If this is not a validation rule error will occur while attempting to save the OPPORTUNITY.
3. Click `Save`
4. To create a quote, see the ['Creating Accounts, Contacts, Opportunities and Quotes'](/handbook/sales/#creating-accounts-contacts-opportunities-and-quotes-in-salesforce) and begin with Step 4. Here is a [video](https://drive.google.com/file/d/142csIZyrzIfSJOSJkIAK6d9c1JwTO_Rq/view?usp=sharing) that hightlights the process. 

### Tracking Sales Qualified Source in the Opportunity

Sales Qualified Source is dimension used when analyzing pipeline creation, lead conversion, sales cycles, and conversion rates. Sales Qualified Source may be different from the Lead Source of an Opportunity, which captures the original source (event, campaign, etc). For example, if a prospect originates from a trial (lead source), that prospect can be qualified by a BDR, SDR, Account Executive, Channel Partner, or the Web (sales qualified source).

The logic for the Sales Qualified Source is as follows:

1. If the Business Development Representative field (Opportunity) is populated, regardless of opportunity owner, the Sales Qualified Source is "BDR Generated"
2. If the Sales Development Representative field (Opportunity) is populated, regardless of opportunity owner, the Sales Qualified Source is "SDR Generated"
3. If both the Business Development Representative and Sales Development Representative fields are NULL and the opportunity owner is:
   * a Regional Director, Account Executive, or Account Manager, the Sales Qualified Source is "AE Generated"
   * a GitLab team member that is not a Regional Director, Account Executive, or Account Manager, the Sales Qualified Source is "Other"
   * an authorized reseller, the Sales Qualified Source is "Channel Generated"
   * the Sales Admin, the Sales Qualified Source is "Web Direct Generated"

### Reseller Opportunities

Opportunities utilizing a reseller require slightly different data:

* Lead/Contact:
The partner record should be converted to their company channel type account. The end user record should be converted to the end user standard account type.

* Opportunity Name:
If the partner is an authorized reseller, rename the opportunity with the partner’s nick-name in front, then a dash.  For instance; if it is a Perforce deal, the opportunity name should start with P4 - (whatever your opportunity name is)  This is important for the workflow that solicits updates from the reseller.

* Account Name:
It is important that opportunities using a reseller are created on the END CUSTOMER’s account, and not the reseller’s account.  The account name on an opportunity is never a reseller.  Resellers do not buy licenses; they purchase them on the behalf of an end customer.  For instance, the account name field on an opportunity should never be SHI.

* Opportunity Owner:
The AE/SAL/Channel Manager who is working the deal with the reseller

* Deal Registrant:
The reseller who registered the deal.

* Associating Contact Roles:
After creating the opportunity, click “New” in the contact section to associate contacts with the opportunity.
 - The primary contact should always be a contact at the end user’s account and not a contact at the reseller.  This is important as resellers come and go, and if we do not capture the contact at the end user account, we will not be able to sell to this account if the reseller ends their relationship with us or with the end account.
 - A reseller contact (say, the sales rep at ReleaseTEAM) can, and should be added to the opportunity with the role of Influencer.  NOTE: A contact that works for a reseller should never be added to an end user account.  For instance an employee of SoftwareOne should be a contact of the SoftwareOne account only, and not the Boeing account.

* Associating Partners to an Opportunity:
After creating the opportunity, click “New” in the Partners section to associate the reseller with the opportunity.
 - You can associate multiple partners with an opportunity if there is more than one reseller involved in the opportunity. This is not uncommon for government opportunities, or opportunities where the customer is asking multiple fulfillment houses (like SHI and SoftwareOne) to fulfill the order.
 - Unofficial resellers should never be marked primary
 - If there are any authorised resellers associated, at least 1 must be marked as primary
    - In order of precedence this would be:
        - The deal registrant
        - The deal fulfiller (i.e.: The one issuing the PO)
        - The distributor

* Opportunity Team List:
Add the reseller user to the Opportunity team list with the role of “Reseller” or else they cannot see the opportunity.

### Opportunity Naming Convention

Opportunities for subscriptions will use the following guidelines:

- **New Business**: [Quantity]
   - [Name of Company]- [Quantity] [Edition]
   - Example: Acme, Inc- 50 Starter

- **Add-On Business (seats only)**:
   - [Name of Company]- Add [Quantity] [Product]
   - Example: Acme, Inc- Add 25 Starter

- **Add-On Business (Upgrade from Starter to Premium)**:
   - [Name of Company]- Upgrade to Ultimate
   - Example: Acme, Inc- Upgrade to Ultimate

- **Renewal Business (no changes)**:
   - [Name of Company]- [Quantity] [Product] Renewal [MM/YY]
   - Example: Acme, Inc- 50 Premium Renewal 01/17

- **Renewal Business + Add On Business (seats)**:
   - [Name of Company]- [Quantity] [Product] Renewal [MM/YY]+ Add [Quantity]
   - Example: Acme, Inc- 50 Premium Renewal 01/17 + Add 25

- **Renewal Business + Upgrade**:
   - [Name of Company]- [Quantity] Upgrade to Premium + Renewal [MM/YY]
   - Example: Acme, Inc- 50 Upgrade to Premium + Renewal 01/17

- **Professional Services**:
   - [Name of Company]- Professional Services [MM/YY]
   - Example: Acme, Inc- Professional Services 06/18

- **Refunds**:
   - [Original Opportunity Name] - REFUND
   - Example: Acme, Inc- 50 Upgrade to Premium + Renewal 01/17 - REFUND

## Opportunity Types

There are three things that can be new or existing:

- Account (organization)
- Subscription (linked to a GitLab instance)
- Amount (dollars paid for the subscription)

That gives 4 types of of opportunities:

1. New account (new account, new subscription, new amount) This type should be used for any new subscription who signs up either through the sales team or via the web portal. Paid training also falls under this type if the organization does not have an enterprise license.
1. New subscription (existing account, new subscription, new amount) If an existing account is purchasing a new license for another GitLab instance, this will be new business.
1. Add-on business (existing account, existing subscription, new amount) This type should be used for any incremental/upsell business sold into an existing subscription division mid term, meaning not at renewal. This may be additional seats for their subscription or an upgrade to their plan. If an existing account is adding a new subscription, this would be new business, not an add-on.
1. Renewal (existing subscription, existing subscription, existing amount) This type should be used for an existing account renewing their license with GitLab. Renewals can have their value increased, decreased, or stay the same.  We capture incremental annual contract value growth/loss as a field in Salesforce.com. Renewal business can be a negative amount if renewed at less than the previous dollars paid for the subscription (renewal rate). Only the part that is more or less than the old amount is IACV, the rest is part of the the renewal opportunity.

**New business** is the combination of new account and new subscription


### Opportunity Stages

To help move sales through the sales process, [here](https://docs.google.com/document/d/1ag7YY9aJ93j0CRZb-DrbfgH3vmHprTEdjG7l3O57xEk/edit) is a list of questions to ask at each stage

**00-Pre Opportunity**- This stage should be used when an opportunity does not meet our opportunity criteria. However, there is a potential for business, and it should be tracked for possible business.
* What to Complete in This Stage:
  * Schedule discovery call with prospect to determine if there is an opportunity to pursue; or
  * If there is no opportunity then the stage would be updated to 0-Unqualified.
* Creating an Opportunity in this stage from Contacts
  * If you are qualifying a contact for a new business opportuity open an opportunity in this stage and use the qualifications questions in the qualifictation section on the opportunity while qualifying the contacts.
  * "Convert" Opportnities from this stage to **0-Pending Acceptance** when the opportunity meets [our criteria for an opportunity](#criteria-for-sales-accepted-opportunity-sao)
  * Follow [our process](#how-to-create-an-opportunity) on creating opportunities from a contact record.

**0-Pending Acceptance**: This is the initial stage once an opportunity is created.
* What to Complete in This Stage:
  * For BDR or SDR sourced opportunities, the opportunity meets [Sales Accepted Opportunity criteria](#criteria-for-sales-accepted-opportunity-sao).
  * The BDR or SDR has scheduled a call via Google Calendar, sent invites, created an event on the account object, named the event: GitLab Introductory Meeting - {{Account Name}}
  * Once it is confirmed that the opportunity meets our Sales Accepted Opportunity criteria, the SAL or AE should move the opportunity to the next stage. The date the opportunity moves from this to the next stage in the sales cycle will populate the `Sales Accepted Date` field on the opportunity record.
  * If the details on the opportunity do not meet our Sales Accepted Opportunity criteria, the SAL or AE should move the opportunity to an `8-Unqualified` stage (this is the only time an opportunity can move into `8-Unqualified` stage).
  * All Opps that are sales assisted must first enter this stage before they can be moved further in the pipeline. If they do not enter this stage at some point you will encounter a validation rule error.

**1-Discovery**: Uncover as much intelligence about the project as you can, which will be confirmed at later stages throughout the sales cycle.
* What to Complete in This Stage:
  * Begin filling out [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification)
  * Send Plan Letter/Recap Email to Attendees- [Example](https://docs.google.com/document/d/16Gurj_MVREmKoqXTdB1F0OQ3eyq1gzbTNU8LNHHuoEM/edit)
  * Scheduled Scoping Call
  * Provide an estimate for the `Expected Number of Users`  and the `Expected Product` for the Opportunity. This information is used to help the customer success team to predict their future workload as well as to help them with their hiring plans. 
  * Should the opportunity progress from `1-Discovery` to the next stage (not 7-Closed Lost or 9-Duplicate), it will be considered a `Sales Qualified Opportunity`. The following values are entered once the opportunity progresses from this stage:
     * `Sales Qualified` is True.
     * `Sales Qualified Date` is the date the opportunity moves from this stage to the next open or won stage.
     * `Initial IACV` captures the value in the `Incremental ACV` field. `Initial IACV` is a snapshot field that will not change, even when the `Incremental ACV` field is updated and will be used for `Deal Size` analysis.

**2-Scoping**: Uncover business challenges/objectives, the competitive landscape, realizing fit.
* What to Complete in This Stage:
  * Complete a Demo (Optional)
  * Schedule a Technical Evaluation Call
  * Confirm and collect new [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification) information.

**3-Technical Evaluation**: Confirming technical requirements. A proof-of-concept (POC) might occur at this stage. This is also the stage to confirm information before a proposal is delivered.
* What to Complete in This Stage:
  *  Enter POC Notes and POC Success Criteria (if applicable) and enter into the POC Notes and POC Success Criteria fields on the opportunity.
  *  Confirm *Technical Requirements, POC Scope*
  *  Confirm *Technical Win/POC Success*
  *  Confirm and collect new [MEDDPIC](/handbook/sales/#capturing-meddpic-questions-for-deeper-qualification) information.

**4-Proposal**: Business and technical challenges and been uncovered and resolved. A proposal is drafted and delivered to the prospect.
* What to Complete in This Stage:
  * Confirm Bill to Information (who will receive the invoices), Sold to Information (who will receive the license key). You should also confirm whether or not the customer will issue a PO, and whether there is a vendor registration form required by the customer.
  * Deliver formal contract to the prospect with complete bill to and sold to information. Remember that quotes with incomplete or incorrect information will be rejected by Deal Desk.
  * An MSA may be delivered separately
  * Clear understanding of purchase/contract review process and a close plan (actions to be taken, named of people to complete actions and dates for each action) documented in the Purchasing Plan field.

**5-Negotiating**: The prospect or customer has received the proposal and is in contract negotiations.
* What to Complete in This Stage:
  * Agreement on business terms
  * All proposals should include the standard GitLab [Terms](/terms/)
  * Determine if customer will be referenceable when the opportunity closes. If the answer is:
       * "Yes" update the `Referenceable Customer` section on the Account object with appropriate reference information   
       * "No" the discussion of being a reference can be revisited at a later date    
  * Modifications will not be accepted to the standard terms for any opportunity that is less than $25k, or for Starter edition.
  * If the above threshold is met, requests for modifications to the standard terms should be sent to Legal through SalesForce, using Chatter. Be sure to include the relevant Account and Opportunity information in the request.
  * If the Account is seeking to use their own paper, requests should be made to legal using the [Legal Issue Tracker](mailto:legal@gitlab.com).

**6-Awaiting Signature**: The prospect or customer has verbally agreed to the terms and conditions outlined in the proposal and has submitted for signature.
* What to Complete in This Stage:
  * Received signed order form, which signals agreement of all pricing and legal terms.
  * Obtain a purchase order, if applicable.
  * Work with GitLab AR to deliver any tax and/or complete any vendor registration processes.
  * Ensure all relevant documents, MSA, PO, and other forms uploaded to SFDC in the Notes and Attachments section of the opportunity record.
  * EULA (End User Licence Agreement) has been accepted by end-user organization (if applicable).
  * If this is a large/strategic account, or if IACV is +$12,000, enter `Closed Won Details` summarizing why we won the opportunity.
  * Subscription created in Zuora.
  * Opportunity has been submitted for Finance approval.

**7-Closed Won**: Congratulations!! The terms have been agreed to by both parties and the quote has been approved by Finance.
* What to Complete in This Stage:
  * Introduce Customer Success/Account Management (if applicable)
  * Set a calendar reminder for 30 day follow up
  * If applicable, initiate Customer Onboarding or Premium Support onboarding

**8-Closed Lost**: An opportunity was lost and the prospect/customer has decided not to pursue the purchase of GitLab.
* What to Complete in This Stage:
  * Select all applicable Closed Lost Reasons
  * In the `Closed Lost Detail`, enter as much detail as you can as to why we lost the deal. For example:
     * If they selected a competitor, why? Was it due to features or pricing?
     * If decided not to move forward with a project, what were the reasons? Did they not understand the value? Was there not a compelling event or reason?
     * Again, please be as thorugh as you can as this information will prove valuable as we learn from these experiences.
  * Please note that for new business deals where the opportunity is with a Large/Strategic account OR the Incremental Annual Contract Value (IACV) is equal or greater than USD 12,000, then a notification will be sent to the [#lost-deals](https://gitlab.slack.com/messages/C8RP2BBA7) Slack channel.
  * Uncover a time for follow up (incumbent solution contract expiration date)
  * Note that if an opportunity is dead/stalled, mark the Stage as 8-Closed Lost. Should the prospect/customer re-engage before 30 days, you can reopen this opportunity. However, if they re-engage beyond 30 days, you will need to create a new opportunity.
  * If the `Closed Lost Reason` is "Merged into another opportunity" please link this closed opportunity to the opportunity you are merging it into by using the `Merged Opportunity` lookup field. Otherwise, you will encounter a validation rule error.

**9-Unqualified**: An opportunity was never qualified.
* What to Complete in This Stage:
  * Update Reason for Closed Lost and add any pertinent notes as to why the opportunity was not qualified.
  * A notification will be sent to BDR or SDR Team Lead and a feedback session should be scheduled between AE and Team Lead.

**10-Duplicate**: A duplicate opportunity exists in the system. This usually happens when a web direct opportunity is won when an existing opportunity already exists in Salesforce. Another reason could be multiple renewals created for the same opportunity. This stage **should not** be used when disqualifying an opportunity or if it is discovered at some point after acceptance that the opportunity is really part of a larger initiative. If the opportunity was accepted, it cannot be marked as a duplicate. Instead, you must mark the opportunity as `8-Closed Lost` and select the appropriate reason. Possible selections could include "Consolidating order - part of another subscription" or "Merged into another opportunity" as reasons why a duplicate opportunity may have been created.

#### Opportunity Stage Movement Considerations
Note that once you qualify an opportunity via our standard qualification process, you cannot revert an opportunity back to the following stages: `00-Pre Opportunity`, `0-Pending Acceptance`, or `9-Unqualified`. If you need to revert an opportunity you've previously qualified to one of these stages, please contact Sales Operations and we can determine why the opportunity (once qualified) is no longer qualified.

#### Reverting an Opportunity to a Previous Stage
If a previously met criteria has become unmet, you are required to revert back to the latest stage where all activites were completed. For example, if a prospect had previously signed off on GitLab from a technical standpoint, requested a quote and has begun contract negotiations, you would set the opportunity to `5-Negotiating`. However, if at any point during the negotiations, additional technical questions or requirements arise that result in a re-evaluation of GitLab's technical capabilities, you would revert the opportunity back to `3-Technical Evaluation`. After the opportuniy has once again met the stage completion criteria, you are able to move the opportunity to either `4-Proposal` if a new order form was created as a result of the additional technical requirements, or back to `5-Negotiating` if no changes were made to the original order form.

### Locking Opportunities as a result of their "At Risk" potential
In order to be in compliance with US Regulations there is a need to screen opportunities against known individuals and organizations who we should not be selling to. In order to meet these regulations opportunities are screened when they are created through a third party application, Visual Compliance. If an opportunity triggers any Red Flags it will be locked until it is reviewed by our legal team and it is deemed that it is okay to move forward with the opportunity. 

## Types of Accounts

### Accounts Created in Salesforce utilizing CE Usage Ping Data
The [CE Usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) provides GitLab with some limited insight into how end users are utilizing the platform. The raw information is cleaned, enriched and then pushed to SFDC as an Account by the Data and Analytics team.

If there is not an existing account match in Salesforce, a new account record will be created with the following information populated:

| SFDC Field | Default Value |
|---|---|
| Account Name |  |
| Account Source | `CE Download` |
| Number of Employees |  |
| Billing Street |  |
| Billing City |  |
| Billing Zip |  |
| Billing Country |  |
| Account Type | `Prospect - CE User` |
| Account Website |  |
| Industry | Populated by Clearbit |
| Active CE Users | Populated by Usage Ping |
| CE Instances | Populated by Usage Ping |
| Account Owner | Sales Admin by Default |
| Using CE | Checked True |

**Process**
1. Sales Team members can use this data to proactively identify `Prospect - CE User` accounts that fit their target segement(s). Accounts owned by `Sales Admin` can be adopted by a Sales Team member changing ownership in Salesforce. The adoption of any `Sales Admin` owned records will trigger an email alert that is sent to the Account Research Specialist for transparency and awareness of what account records have been claimed.
2. The Account Research Specialist will be responsible for reviewing the `Prospect - CE User` accounts on a regular basis to determine additional account records that should be worked either by a Sales Team member or Outbound SDR.
3. When an account record has been identified for follow up, the Account Research Specialist will work with the appropriate Regional Director (RD) to determine Outbound SDR assignment based on work load and available capacity.
4. The assigned Outbound SDR will work the `Prospect - CE User` account the same as any other known `CE User` account leveraging the tools at their disposal (DiscoverOrg, LinkedIn Sales Navigator, etc) to add contacts to the account record and populate the firmographic profile of the account.

## Reporting

### Marketing Reports

The Online Growth team is in charge of updating reports and dashboards for marketing meetings. Our key metric is pipe-to-spend.  Pipe compared to marketing spend shows how effective our marketing is in filling the pipeline. This report will include all pipe, not just large and up, and is better than cost per lead as it will account for each type of lead in our hybrid sales model and show the value of leads in larger organizations as they generate more pipe. More information on pipe-to-spend as a marketing metric can be found in [Salesforce](https://success.salesforce.com/ideaView?id=087300000006thQ) and [Marketo](https://blog.marketo.com/2012/08/my-favourite-marketing-metric.html). Currently we are using the Full Path revenue model to show all Touchpoints in a path.  

Marketing metrics sources are here:
- [Pipe](https://na34.salesforce.com/00O61000004IgAu) used to account for pipeline using Full Path model. This will be divided by spend. The SST for pipe-to-spend will be in Looker.
- [Sales Pipeline Looker dashboard](https://gitlab.looker.com/dashboards/27)
- [Web Traffic by source](https://analytics.google.com/analytics/web/#report/acquisition-channels/a37019925w65271535p67064032/)
- [Online Growth Web Metrics Dashboard](https://datastudio.google.com/open/1lSOfaMEBPVpFtLCpFuzvV3I_DfRdtngB)
- [SCLAU Dashboard](https://na34.salesforce.com/01Z61000000Qbm2)

### Report on SCLAU Opportunity Creation

An area of focus is the [large and strategic sales segments](/handbook/sales/#market-segmentation). For visibility into performance in these market segments we use a [Large and Up Opportunity Performance dashboard](https://na34.salesforce.com/01Z61000000J0h1) in salesforce.com. It tracks the measures [pending acceptance](#customer-lifecycle/) opportunities, [SAO](#customer-lifecycle/), and [SQO](#customer-lifecycle/).

The dashboard analyzes opportunity measures by the dimensions `initial source type` [sales segmentation](#segmentation) and [sales qualification source](/handbook/sales/#tracking-sales-qualified-source-in-the-opportunity). We set our demand generation targets based on SAO count in large and up segments by `initial source type` and `sales qualification source` as follows:

- BDR Generated - opportunity is created by a BDR qualifying inbound demand. Sales qualification source is `BDR Generated`.
- SDR Generated - opportunity is created by SDR prospecting. Sales qualification source is `SDR Generated`.
- AE Generated, Marketing assisted - opportunity is created by an AE or SAL and is associated with a contact that has responded to one or more marketing programs. Sales qualification source is `AE generated` and `Initial source` is not `AE Generated`.
- AE Generated, Cold calling - opportunity is created by an AE or SAL but is not associated with any contacts that have responded to marketing programs. Sales qualification source is `AE generated` and `Initial source` is `AE Generated`.


#### Using Bizible Fields in Salesforce to Report Attribution
 In 4Q18, we are making updates to the Bizible Channel rules, but currently, these channels and subchannels are pulled into Salesforce and can be further filtered by using `medium` for those channels with overlap or with `Ad Campaign name` to search for specific UTMs or campaigns:

| Bizible Online Channel or subchannel | Type of marketing |SFDC Marketing Channel-path |
|---|---|---|
|`CPC`|Google Adwords or other Paid Search|CPC.Adwords|
|`Display`|Display ads in Doubleclick, Terminus, etc|Display|
|`Paid Social`|Ads in Facebook or LinkedIn |Paid Social.[Name of site]|
|`Organic`|Organic search|Marketing Site.Organic|
|`Other`|Not specifically defined |[Name of channel].Other|
|`Partners`|Google or events|	Marketing Site.Web Direct|
|`Email`|Nurture, Newsletter, Outreach emails|Email.[Name of email type]|
|`Field Event`|From Field event, will show Salesforce campaign as touchpoint source|Email.[Field Event]|
|`Conference`|From conference, will show Salesforce campaign as touchpoint source|Conference|
|`Social`|Any referral from any defined social media site| Social.[Name of site]|
|`Sponsorship`|Paid sponsorships, Display, and Demand gen as well as Terminus|Sponsorship|
|`Web Direct`|Unknown or direct (NOTE: this is not the same as Web direct/self-serve in SFDC, this is a Web referral where the original source was not captured)|Marketing Site.Web Direct|
|`Web Referral`|Referral from any site not otherwise defined|Marketing Site.Web Referral|

