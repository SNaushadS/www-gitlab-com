- [ ] Is the post time sensitive? If so, please mention `@rebecca` to give her a heads up ASAP.

- [ ] When is the desired publish date/milestone?

- [ ] Have you checked out the [blog handbook](https://about.gitlab.com/handbook/marketing/blog/)? See the [Editorial reviews section](https://about.gitlab.com/handbook/marketing/blog/#editorial-reviews) specifically.

- [ ] Have you created a merge request for this post? Please set it to close this issue automatically.

/label ~"blog post"
